package nl.makertim.iminelauncher;

public enum LogLevel {
	OFF, FATAL, ERROR, WARN, INFO, DEBUG, TRACE, ALL;
}
