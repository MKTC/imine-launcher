package nl.makertim.iminelauncher;

import java.io.File;
import java.util.function.Consumer;

import nl.makertim.iminelauncher.pack.Pack;
import nl.makertim.iminelauncher.util.PackUtil;

public class PackDownloader {

	public static void installPack(Pack pack, Consumer<Double> statusUpdate) throws Exception {
		File packFolder = pack.getDirectory();
		Logger.getLogger().debug(pack.getClass().getSimpleName() + " directory at: " + packFolder.getAbsolutePath());
		packFolder.mkdirs();
		pack.installClient(packFolder, statusUpdate);
		PackUtil.injectPack(pack);
	}
}
