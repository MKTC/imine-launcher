package nl.makertim.iminelauncher;

import java.io.File;
import java.io.IOException;
import java.net.URI;

public enum OS {

	WINDOWS('w'), LINUX('l', 'u'), MAC('m', 'o'), OTHER('\n');

	private char firstChar = 0;
	private char altChar = 0;

	OS(char firstChar) {
		this.firstChar = firstChar;
	}

	OS(char firstChar, char altChar) {
		this(firstChar);
		this.altChar = altChar;
	}

	public char getFirstChar() {
		return firstChar;
	}

	public char getAltChar() {
		return altChar;
	}

	private boolean hasAlt() {
		return altChar != 0;
	}

	public static OS getOS() {
		char osChar = Character.toLowerCase(System.getProperty("os.name").charAt(0));
		for (OS os : OS.values()) {
			if (os.getFirstChar() == osChar) {
				return os;
			}
			if (os.hasAlt() && os.getAltChar() == osChar) {
				return os;
			}
		}
		return OTHER;
	}

	public String getJavaDir() {
		String separator = System.getProperty("file.separator");
		String path = System.getProperty("java.home") + separator + "bin" + separator;
		if ((this == WINDOWS) && (new File(path + "java.exe").isFile())) {
			return path + "javaw.exe";
		}
		return path + "java";
	}

	public void openFolder(File path) {
		String absolutePath = path.getAbsolutePath();
		if (this == OS.WINDOWS) {
			String cmd = String.format("cmd.exe /C start \"Open file\" \"%s\"", absolutePath);
			try {
				Runtime.getRuntime().exec(cmd);
				return;
			} catch (IOException e) {
				Logger.getLogger().error("Couldn't open " + path + " through cmd.exe", e);
			}
		} else if (this == OS.MAC) {
			try {
				Runtime.getRuntime().exec(new String[]{"/usr/bin/open", absolutePath});
				return;
			} catch (IOException e) {
				Logger.getLogger().error("Couldn't open " + path + " through /usr/bin/open", e);
			}
		}
		try {
			Class<?> desktopClass = Class.forName("java.awt.Desktop");
			Object desktop = desktopClass.getMethod("getDesktop", new Class[0]).invoke(null);
			desktopClass.getMethod("browse", new Class[]{URI.class}).invoke(desktop, path.toURI());
		} catch (Throwable e) {
			Logger.getLogger().error("Couldn't open " + path + " through Desktop.browse()", e);
		}
	}
}
