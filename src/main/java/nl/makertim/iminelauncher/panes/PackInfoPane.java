package nl.makertim.iminelauncher.panes;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.OS;
import nl.makertim.iminelauncher.PackSelectionManager;
import nl.makertim.iminelauncher.pack.Pack;
import nl.makertim.iminelauncher.pack.PackStatus;
import nl.makertim.iminelauncher.util.PackUtil;
import nl.makertim.iminelauncher.util.ScheduleUtil;
import nl.makertim.iminelauncher.util.StringUtil;

public class PackInfoPane extends Pane {

	private static final Color BACKGROUND = new Color(0.11, 0.13, 0.15, 1);
	private static final String PACK_FORMAT = "Currently selected: %s";
	private static final Font FONT = Font.font("Arial", 20);
	private static final Color COLOR = Color.WHITESMOKE;

	private VBox lines;
	private HBox buttons;
	private HBox buttons2;
	private Button update;
	private Button startMinecraft;
	private Button openFolder;
	private Button removePack;
	private Button restore;
	private Text status;
	private Text packName;

	private boolean check = true;
	private boolean done = true;

	public PackInfoPane() {
		setMinSize(300, 150);
		setBackground(new Background(new BackgroundFill(BACKGROUND, CornerRadii.EMPTY, Insets.EMPTY)));

		Pack selectedPack = PackSelectionManager.getInstance().getSelectedPack();
		if (selectedPack == null) {
			return;
		}

		lines = new VBox(3);
		buttons = new HBox(2);
		buttons2 = new HBox(2);
		update = new Button("Update");
		packName = new Text(String.format(PACK_FORMAT, selectedPack.getName()));
		status = new Text("Retrieving info...");

		packName.setFont(FONT);
		status.setFont(FONT);
		packName.setFill(COLOR);
		status.setFill(COLOR);

		buttons.getChildren().add(update);
		lines.getChildren().addAll(packName, status, new StackPane(buttons), buttons2);
		getChildren().add(lines);
		registerEvents();
		getPackStatus(selectedPack);
	}

	private void registerEvents() {
		update.setOnMouseClicked(click -> update());
		PackSelectionManager.getInstance().registerPackSelectChange(pack -> {
			Logger.getLogger().info("Selecting pack " + pack.getName());
			packName.setText(String.format(PACK_FORMAT, pack.getName()));
			getPackStatus(pack);
		});
	}

	private void busy() {
		try {
			Alert alarm = new Alert(AlertType.WARNING);
			alarm.setHeaderText("Please wait a moment, there is a other process running.");
			alarm.showAndWait();
		} catch (Throwable thr) {
			Logger.getLogger().warn("Please wait a moment, there is a other process running.");
			thr.printStackTrace();
		}
	}

	private void update() {
		if (!ScheduleUtil.isWorking()) {
			ProgressBar bar = new ProgressBar(0);
			lines.getChildren().add(bar);
			bar.setPrefWidth(lines.getWidth());
			new Thread(() -> {
				Pack pack = PackSelectionManager.getInstance().getSelectedPack();
				PackUtil.updatePack(pack, bar::setProgress);
				Platform.runLater(() -> {
					lines.getChildren().remove(bar);
					getPackStatus(pack);
				});
			}).start();
		} else {
			busy();
		}
	}

	private void onStartMinecraft() {
		if (!ScheduleUtil.isWorking()) {
			PackUtil.startMinecraft(PackSelectionManager.getInstance().getSelectedPack());
		} else {
			busy();
		}
	}

	private void openFolder(Pack pack) {
		Logger.getLogger().debug("Opening folder for " + pack.getName());
		OS.getOS().openFolder(pack.getDirectory());
	}

	private void statusWaiting() {
		check = true;
		new Thread(() -> {
			try {
				int i = 0;
				if (check) {
					Platform.runLater(() -> status.setText("Retrieving info"));
				}
				while (check) {
					++i;
					i %= 4;
					int j = i;
					Platform.runLater(() -> {
						if (status.getText().startsWith("Retrieving")) {
							status.setText("Retrieving info" + StringUtil.repeat(j, "."));
						}
					});
					Thread.sleep(333);
				}
			} catch (Exception ex) {
			}
		}).start();
	}

	private void stopWaiting() {
		check = false;
	}

	public void updatePackStatus() {
		getPackStatus(PackSelectionManager.getInstance().getSelectedPack());
	}

	private void getPackStatus(Pack pack) {
		if (ScheduleUtil.isWorking()) {
			busy();
			return;
		}
		ScheduleUtil.setWorking();
		Platform.runLater(() -> {// TODO: Cleanup
			buttons.getChildren().remove(restore);
			restore = null;
			new Thread(() -> {
				statusWaiting();
				PackStatus status = pack.getStatus();
				stopWaiting();
				Platform.runLater(() -> {
					String msg = status.message;
					switch (status) {
					case NEW:
						update.setText("Install");
						File backup = new File(
								pack.getDirectory().getAbsolutePath().replace(pack.getId(), "backup_" + pack.getId()));
						if (backup.exists()) {
							restore = new Button("Restore");
							restore.setOnMouseClicked(click -> {
								try {
									FileUtils.moveDirectory(backup, pack.getDirectory());
								} catch (IOException ex) {
									ex.printStackTrace();
								}
								getPackStatus(pack);
							});
							buttons.getChildren().add(restore);
						}
						break;
					}
					if (startMinecraft != null) {
						buttons.getChildren().remove(startMinecraft);
						startMinecraft = null;
					}
					if (openFolder != null) {
						buttons2.getChildren().remove(openFolder);
						openFolder = null;
					}
					if (removePack != null) {
						buttons2.getChildren().remove(removePack);
						removePack = null;
					}
					if (status == PackStatus.UP_TO_DATE || status == PackStatus.OTHER) {
						update.setText("Force update");
						startMinecraft = new Button("Start minecraft");
						startMinecraft.setOnMouseClicked(click -> onStartMinecraft());
						openFolder = new Button("Open directory");
						openFolder.setOnMouseClicked(click -> openFolder(pack));
						removePack = new Button("Remove pack");
						removePack.setOnMouseClicked(click -> {
							pack.removeClient(pack.getDirectory());
							Logger.getLogger().info("Removed data from " + pack.getName());
							getPackStatus(pack);
						});
						buttons.getChildren().add(startMinecraft);
						buttons2.getChildren().addAll(openFolder, removePack);
					} else {
						update.setText("Update");
					}
					this.status.setText("Status: " + msg);
				});
			}).start();
			ScheduleUtil.setDone();
		});
	}
}
