package nl.makertim.iminelauncher.panes;

import java.net.URL;

import com.github.rjeschke.txtmark.Processor;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.util.WebUtil;

public class WebPane extends Pane {

	private static final String INFO_URL = "http://git.imine.nl/imine/Launcher/raw/master/webfiles/Info.md";
	private static final String DOC = "<!DOCTYPE html><html><body>%s%s</body></html>";
	private static final String AD = //
			"<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>\n" + //
					"<script>\n" + //
					"  (adsbygoogle = window.adsbygoogle || []).push({\n" + //
					"    google_ad_client: \"ca-pub-4324032139771303\",\n" + //
					"    enable_page_level_ads: true\n" + //
					"  });\n" + //
					"</script>";
	private static final Color BACKGROUND = new Color(0.11, 0.13, 0.15, 1);

	private WebView view;
	private boolean init = true;

	public WebPane() {
		setPrefSize(Short.MAX_VALUE, Short.MAX_VALUE);
		setBackground(new Background(new BackgroundFill(BACKGROUND, CornerRadii.EMPTY, Insets.EMPTY)));
		try {
			fillInfo();
		} catch (Exception ex) {
			Logger.getLogger().warn("WebPane", ex);
		}
		widthProperty().addListener(width -> {
			if (!init) {
				view.setMinSize(getWidth(), getHeight());
				view.setMaxSize(getWidth(), getHeight());
			}
			init = false;
		});
	}

	private void fillInfo() throws Exception {
		String md = WebUtil.getResponseWith(new URL(INFO_URL), "\n");
		String content = Processor.process(md);
		view = new WebView();
		view.setPrefWidth(900);
		view.getStyleClass().add("browers");
		URL css = getClass().getResource("Info.css");
		if (css == null) {
			css = getClass().getResource("/Info.css");
		}
		view.getEngine().setUserStyleSheetLocation(css.toString());
		view.getEngine().loadContent(String.format(DOC, AD, content));
		getChildren().add(view);
	}

}
