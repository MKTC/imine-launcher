package nl.makertim.iminelauncher.panes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.PackSelectionManager;
import nl.makertim.iminelauncher.dialog.LauncherSettingsDialog;
import nl.makertim.iminelauncher.dialog.RepoManagementDialog;
import nl.makertim.iminelauncher.element.PackSelector;
import nl.makertim.iminelauncher.pack.Pack;

public class PackListPane extends VBox {

	private static final Color BACKGROUND = new Color(0.09, 0.11, 0.13, 1);

	private ScrollPane scroll;
	private HBox topRow;

	public PackListPane() {
		super(10);
		setMinSize(300, 600);
		setBackground(new Background(new BackgroundFill(BACKGROUND, CornerRadii.EMPTY, Insets.EMPTY)));
		addConfigRow();
		updatePackList();
	}

	private void addConfigRow() {
		topRow = new HBox(10);
		topRow.setAlignment(Pos.TOP_RIGHT);
		topRow.setPadding(new Insets(2, 2, -8, 0));
		ImageView settings = new ImageView("gear.png");
		settings.setFitHeight(20);
		settings.setFitWidth(20);
		settings.setOnMouseClicked(click -> new LauncherSettingsDialog());
		ImageView add = new ImageView("add.png");
		add.setFitHeight(20);
		add.setFitWidth(20);
		add.setOnMouseClicked(click -> new RepoManagementDialog());
		topRow.getChildren().addAll(add, settings);
		getChildren().add(topRow);
	}

	public void updatePackList() {
		VBox packs = new VBox(0);
		PackSelectionManager packManager = PackSelectionManager.getInstance();
		Pack selected = packManager.getSelectedPack();
		packs.getChildren().clear();
		if (scroll != null) {
			getChildren().remove(scroll);
		}
		double d = 0;
		double inc = packManager.getPacks().size();
		double scrollPos = 0;
		for (Pack pack : packManager.getPacks()) {
			PackSelector clickablePack = new PackSelector(pack);
			if (pack.equals(selected)) {
				clickablePack.setSelected(true);
				scrollPos = d;
			}
			Logger.getLogger().debug(String.format("Adding pack '%s'", pack.getName()));
			packs.getChildren().add(clickablePack);
			d += inc;
		}
		scroll = new ScrollPane(packs);
		scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
		scroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scroll.setPrefHeight(Short.MAX_VALUE);
		scroll.setVvalue(scrollPos);

		// Scroll speed?
		packs.setOnScroll(scrollEvent -> {
			if (scrollEvent.getDeltaY() > 0) {
				scroll.setVvalue(scroll.getVvalue() - 0.5);
			} else {
				scroll.setVvalue(scroll.getVvalue() + 0.5);
			}
		});

		scroll.setFitToHeight(true);
		scroll.setFitToWidth(true);
		getChildren().add(scroll);
	}

	public void registerHeight(Scene scene) {
		getScene().heightProperty().addListener(height -> {
			scroll.setMaxHeight(getScene().getHeight()
					- (topRow.getHeight() + topRow.getPadding().getTop() + topRow.getPadding().getBottom()
							+ scroll.getPadding().getTop() + scroll.getPadding().getBottom() + 8));
		});
		scroll.setMaxHeight(getScene().getHeight()
				- (topRow.getHeight() + topRow.getPadding().getTop() + topRow.getPadding().getBottom()
						+ scroll.getPadding().getTop() + scroll.getPadding().getBottom() + 8));
	}

}
