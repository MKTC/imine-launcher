package nl.makertim.iminelauncher.panes;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import nl.makertim.iminelauncher.mojang.AccountData;
import nl.makertim.iminelauncher.mojang.LauncherProfileGetter;
import nl.makertim.iminelauncher.mojang.LauncherProfileInjector;

public class PlayerPane extends HBox {

	private static final Color BACKGROUND = new Color(0.11, 0.13, 0.15, 1);

	private List<Pane> accounts = new ArrayList<>();

	public PlayerPane() {
		super(8);
		setAlignment(Pos.BOTTOM_LEFT);
		setPadding(new Insets(0, 0, 8, 8));
		setPrefSize(Short.MAX_VALUE, 150);
		setBackground(new Background(new BackgroundFill(BACKGROUND, CornerRadii.EMPTY, Insets.EMPTY)));
		updatePlayerInfo();
	}

	public void updatePlayerInfo() {
		Platform.runLater(() -> {
			getChildren().clear();
			addPlayers();
		});
	}

	private void addPlayers() {
		List<AccountData> accounts = LauncherProfileGetter.getAccounts();
		String selectedUser = LauncherProfileGetter.getSelectedAccount();
		for (AccountData account : accounts) {
			ImageView head = new ImageView(
					"https://crafatar.com/avatars/" + account.getUuid().toString().replaceAll("-", ""));
			VBox pane = new VBox(head);
			pane.setAlignment(Pos.CENTER);
			Label username = new Label(account.getDisplayName());
			Label mail = new Label("");

			username.setTextFill(Color.WHITESMOKE);
			mail.setTextFill(Color.DARKGRAY);

			pane.hoverProperty().addListener(hover -> {
				if (pane.isHover()) {
					mail.setText(account.getUsername());
					mail.setTextFill(Color.WHITESMOKE);
				} else {
					mail.setText("");
					mail.setTextFill(Color.DARKGRAY);
				}
			});

			pane.getChildren().addAll(username, mail);
			head.setFitHeight(80);
			head.setFitWidth(80);

			pane.setOnMouseClicked(click -> {
				for (Pane acc : this.accounts) {
					setSelected(acc, false);
				}
				setSelected(pane, true);
				LauncherProfileInjector.selectUser(account);
			});

			setSelected(pane, account.getUsername().equalsIgnoreCase(selectedUser));
			this.accounts.add(pane);
			getChildren().add(pane);
		}
	}

	private void setSelected(Pane pane, boolean selected) {
		if (selected) {
			pane.setBorder(new Border(
					new BorderStroke(Color.ORANGE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));
		} else {
			pane.setBorder(new Border(
					new BorderStroke(BACKGROUND, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));
		}
	}

}
