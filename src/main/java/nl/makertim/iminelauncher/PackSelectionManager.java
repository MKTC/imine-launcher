package nl.makertim.iminelauncher;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;

import com.google.gson.*;

import nl.makertim.iminelauncher.mojang.LauncherProfileGetter;
import nl.makertim.iminelauncher.mojang.LauncherProfileInjector;
import nl.makertim.iminelauncher.pack.*;
import nl.makertim.iminelauncher.prelauncher.Console;
import nl.makertim.iminelauncher.util.FileUtil;
import nl.makertim.iminelauncher.util.WebUtil;

public class PackSelectionManager {

	public static final String JSON = "http://git.imine.nl/imine/Launcher/raw/master/webfiles/packs.json";
	private static PackSelectionManager INSTANCE;

	private List<Pack> packs;
	private List<Consumer<Pack>> onPackChangeEvent;
	private int selectedIndex = 0;

	private PackSelectionManager() {
		packs = new ArrayList<>();
		onPackChangeEvent = new ArrayList<>();
		loadUrls();
		addDefaults();
		autoSelectLatest();
	}

	public static PackSelectionManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new PackSelectionManager();
		}
		return INSTANCE;
	}

	public void addDefaults() {
		getPacks().add(0, VanillaPack.VANILLA_PACK);
		getPacks().add(1, VanillaForge.FORGE_PACK);
	}

	public void autoSelectLatest() {
		String mcSelectedPack = LauncherProfileGetter.getSelectedProfile();
		for (Pack pack : getPacks()) {
			if (pack.getId().equalsIgnoreCase(mcSelectedPack)) {
				selectPack(pack);
				return;
			}
		}
		selectPack(getSelectedPack());
	}

	public void loadUrls() {
		File urls = new File(FileUtil.getApplicationDataFolder(), "repos.txt");
		if (!urls.exists()) {
			try {
				FileUtils.write(urls, JSON, Charset.defaultCharset());
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		List<String> urlList;
		try {
			urlList = FileUtils.readLines(urls, Charset.defaultCharset());
		} catch (IOException e) {
			urlList = new ArrayList<>();
			urlList.add(JSON);
		}
		for (String url : urlList) {
			try {
				downloadPack(url);
			} catch (Exception ex) {
				Logger.getLogger().warn("Something went wrong with downloading packs from " + url + " ["
						+ ex.getClass().getSimpleName() + "]",
					ex);
			}
		}
	}

	private void downloadPack(String url) {
		String json = WebUtil.getResponse(url);
		if (json == null || json.isEmpty()) {
			Logger.getLogger().error("No packs found! Where is the internet?");
			return;
		}
		Logger.getLogger().debug("Downloading from " + url);
		try {
			JsonElement parsedJson = new JsonParser().parse(json);
			JsonObject root = parsedJson.getAsJsonObject();
			if (root.has("packs")) {
				JsonArray packUrls = root.get("packs").getAsJsonArray();
				for (JsonElement packUrl : packUrls) {
					getPackFromJson(packUrl.getAsString());
				}
			} else {
				getPackFromJson(url);
			}
			if (root.has("latestVersion")) {
				Version latestVersion = new Version(root.get("latestVersion").getAsString());
				if (latestVersion.compareTo(Main.VERSION) > 0) {
					Logger.getLogger().warn("#########");
					Logger.getLogger().warn("YOU ARE NOT RUNNING NEWEST VERSION OF THE LAUNCHER");
					Logger.getLogger().warn("YOUR VERSION = v" + Main.VERSION);
					Logger.getLogger().warn("NEWEST VERSION = v" + latestVersion);
					Logger.getLogger().warn("#########");
					Console.INSTANCE.showDownloadButton();
				}
			}
		} catch (JsonSyntaxException ex) {
			Logger.getLogger().warn("Packs from " + url + " are not loading", ex);
		} catch (Exception ex) {
			Logger.getLogger().error("Error while getting stuff from the internet", ex);
		}
	}

	private Pack getPackFromJson(String packUrl) {
		Pack pack = null;
		String jsonPack = null;
		try {
			jsonPack = WebUtil.getResponse(packUrl);
			JsonObject packObject = new JsonParser().parse(jsonPack).getAsJsonObject();
			if (packObject.has("giturl")) {
				pack = getGitPack(packObject);
			} else if (packObject.has("zipurl")) {
				pack = getZipPack(packObject);
			}

			if (pack == null) {
				throw new IllegalArgumentException("Pack type not supported. " + packObject);
			}
			Logger.getLogger().debug(String.format("Found pack %s", pack));
			for (Pack pack1 : packs) {
				if (pack1.getId().equalsIgnoreCase(pack.getId())) {
					jsonPack = null;
					throw new IllegalArgumentException("GitPack " + pack.getId() + " already added");
				}
			}
			this.packs.add(pack);
		} catch (Exception ex) {
			Logger.getLogger().warn("GitPack error " + ex.getClass().getName() + " -> " + ex.getLocalizedMessage()
					+ " '" + (jsonPack == null ? packUrl.toString() : jsonPack) + "'",
				ex);
		}
		return pack;
	}

	private Pack getZipPack(JsonObject packObject) {
		String packID = packObject.get("packID") == null ? "unknown" : packObject.get("packID").getAsString();
		String name = packObject.get("name") == null ? "unknown" : packObject.get("name").getAsString();
		String zipUrl = packObject.get("zipurl") == null ? "unknown" : packObject.get("zipurl").getAsString();
		String version = packObject.get("version") == null ? "unknown" : packObject.get("version").getAsString();
		String imgUrl = packObject.get("imgurl") == null ? "unknown" : packObject.get("imgurl").getAsString();
		String forgeVersion = packObject.get("forgeVersion") == null ? "unknown"
				: packObject.get("forgeVersion").getAsString();
		String minRamSize = packObject.get("minRamsize") == null ? "1024M" : packObject.get("minRamsize").getAsString();
		String prefRamSize = packObject.get("prefRamsize") == null ? "2048M"
				: packObject.get("prefRamsize").getAsString();
		return new ZipPack(packID, name, zipUrl, version, imgUrl, forgeVersion, minRamSize, prefRamSize);
	}

	private Pack getGitPack(JsonObject packObject) {
		String packID = packObject.get("packID") == null ? "unknown" : packObject.get("packID").getAsString();
		String name = packObject.get("name") == null ? "unknown" : packObject.get("name").getAsString();
		String gitUrl = packObject.get("giturl") == null ? "unknown" : packObject.get("giturl").getAsString();
		String versionUrl = packObject.get("versionurl") == null ? "unknown"
				: packObject.get("versionurl").getAsString();
		String imgUrl = packObject.get("imgurl") == null ? "unknown" : packObject.get("imgurl").getAsString();
		String forgeVersion = packObject.get("forgeVersion") == null ? "unknown"
				: packObject.get("forgeVersion").getAsString();
		String minRamSize = packObject.get("minRamsize") == null ? "1024M" : packObject.get("minRamsize").getAsString();
		String prefRamSize = packObject.get("prefRamsize") == null ? "2048M"
				: packObject.get("prefRamsize").getAsString();
		return new GitPack(packID, name, gitUrl, versionUrl, imgUrl, forgeVersion, minRamSize, prefRamSize);
	}

	public List<Pack> getPacks() {
		return packs;
	}

	public void selectPack(Pack pack) {
		for (int i = 0; i < packs.size(); i++) {
			if (pack.getId().equals(packs.get(i).getId())) {
				selectedIndex = i;
				break;
			}
		}
		LauncherProfileInjector lpi = new LauncherProfileInjector(pack);
		lpi.selectPack();
		for (Consumer<Pack> eventChanger : onPackChangeEvent) {
			eventChanger.accept(pack);
		}
	}

	public Pack getSelectedPack() {
		if (packs.isEmpty()) {
			return null;
		}
		return packs.get(selectedIndex);
	}

	public void registerPackSelectChange(Consumer<Pack> listener) {
		onPackChangeEvent.add(listener);
	}

}
