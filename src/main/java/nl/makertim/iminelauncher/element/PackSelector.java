package nl.makertim.iminelauncher.element;

import java.io.File;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.DirectoryChooser;
import nl.makertim.iminelauncher.Launcher;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.PackSelectionManager;
import nl.makertim.iminelauncher.dialog.PackInfoDialog;
import nl.makertim.iminelauncher.pack.Pack;
import nl.makertim.iminelauncher.util.ImgUtil;

public class PackSelector extends StackPane {

	private final Pack pack;
	private ImageView image;
	private Rectangle overlay;

	public PackSelector(Pack pack) {
		this.pack = pack;
		build();
		PackSelectionManager.getInstance()
				.registerPackSelectChange(packSelected -> setSelected(packSelected.getId().equals(this.pack.getId())));

	}

	private void build() {
		File imgFile = ImgUtil.getCachedImg(pack.getImgURL());

		image = new ImageView(imgFile.toURI().toString());
		image.setFitWidth(290);
		image.setPreserveRatio(true);
		image.setSmooth(true);
		image.setCache(true);
		image.setOnMouseClicked(click -> {
			if (click.getButton() == MouseButton.PRIMARY) {
				onNameClick();
			} else {
				onRClick(click);
			}
		});

		overlay = new Rectangle(image.getBoundsInParent().getWidth(), image.getBoundsInParent().getHeight());
		Platform.runLater(() -> {
			overlay.setWidth(image.getBoundsInParent().getWidth());
			overlay.setHeight(image.getBoundsInParent().getHeight());
		});
		overlay.setFill(Color.TRANSPARENT);

		getChildren().addAll(overlay, image);
	}

	public void setSelected(boolean selected) {
		if (selected) {
			setBorder(new Border(new BorderStroke(Color.ORANGE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY,
					new BorderWidths(10, 0, 10, 0))));
			overlay.setFill(Color.ORANGE);
		} else {
			setBorder(null);
			overlay.setFill(Color.TRANSPARENT);
		}
	}

	public void onRClick(MouseEvent click) {
		ContextMenu menu = new ContextMenu();

		MenuItem name = new MenuItem(pack.getName());
		SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();
		MenuItem info = new MenuItem("Info");
		MenuItem changelog = new MenuItem("Changelog");
		MenuItem server = new MenuItem("Install server");

		name.setOnAction(clickItem -> onNameClick());
		info.setOnAction(clickItem -> onInfoClick());
		changelog.setOnAction(clickItem -> onChangelogClick());
		server.setOnAction(clickItem -> onServerClick());

		menu.getItems().addAll(name, separatorMenuItem, info, changelog, server);

		menu.setAnchorX(Launcher.getInstance().getStage().getX() + click.getSceneX());
		menu.setAnchorY(Launcher.getInstance().getStage().getY() + click.getSceneY());
		menu.show(Launcher.getInstance().getStage());
	}

	private void alert(String message) {
		Alert alert = new Alert(Alert.AlertType.WARNING);
		alert.setContentText(message);
		alert.showAndWait();
	}

	public void onServerClick() {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setInitialDirectory(new File(System.getProperty("user.home", ".")));
		File newLocation = chooser.showDialog(getScene().getWindow());
		if (newLocation != null) {
			if (!newLocation.isDirectory()) {
				alert("Servers can only be installed in directory's.");
				return;
			}
			if ((newLocation.listFiles() != null && newLocation.listFiles().length != 0)
					|| newLocation.listFiles() == null) {
				alert("Servers shouldn't be installed in non empty directory's.");
				return;
			}
			pack.installServer(newLocation,
				progress -> Logger.getLogger().info("Downloading server " + progress + "%"));
			Logger.getLogger().info("Server installed @ " + newLocation.getAbsoluteFile());
		}
	}

	public void onInfoClick() {
		new PackInfoDialog(PackInfoDialog.PopupType.INFO, pack).show();
	}

	public void onChangelogClick() {
		new PackInfoDialog(PackInfoDialog.PopupType.COMMITS, pack).show();
	}

	public void onNameClick() {
		PackSelectionManager.getInstance().selectPack(pack);
	}
}
