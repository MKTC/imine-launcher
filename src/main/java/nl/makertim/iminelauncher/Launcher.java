package nl.makertim.iminelauncher;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import nl.makertim.iminelauncher.panes.PackInfoPane;
import nl.makertim.iminelauncher.panes.PackListPane;
import nl.makertim.iminelauncher.panes.PlayerPane;
import nl.makertim.iminelauncher.panes.WebPane;
import nl.makertim.iminelauncher.prelauncher.Console;

public class Launcher extends Application {

	private static Launcher instance;
	private PackInfoPane info;
	private PackListPane packs;
	private PlayerPane playerPane;
	private Stage stage;

	public Launcher() {
		Logger.getLogger().debug("Creating a instance of the view.");
		instance = this;
	}

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		Logger.getLogger().info("Starting view");
		stage.getIcons().add(new Image("iMineIcon.png"));
		Scene scene = new Scene(layout(), 1200, 600);
		packs.registerHeight(scene);
		scene.getStylesheets().add("scrollpane.css");

		stage.setTitle("iMine Modpack Manager v" + Main.VERSION);

		stage.setScene(scene);
		stage.centerOnScreen();
		stage.show();
		stage.setOnCloseRequest(event -> Console.INSTANCE.dispose());
		Platform.runLater(() -> {
			if (System.getProperty("sun.arch.data.model").equals("32")
					|| !System.getProperty("sun.arch.data.model").equals("64")) {
				Logger.getLogger().warn(System.getProperty("sun.arch.data.model") + " is not supported");
				try {
					Alert alarm = new Alert(Alert.AlertType.WARNING);
					alarm.setHeaderText("Wrong version of java detected!");
					alarm.setContentText("Java 64bits is required!");
					alarm.show();
				} catch (Throwable thr) {
					thr.printStackTrace();
				}
			}
		});
	}

	public void syncSettings() {
		PackSelectionManager.getInstance().autoSelectLatest();
		Launcher.getInstance().getPlayerPane().updatePlayerInfo();
	}

	private Pane layout() {
		HBox horizontal = new HBox(0);
		VBox contentLogin = new VBox(0);
		HBox bottom = new HBox(0);

		WebPane web = new WebPane();
		info = new PackInfoPane();
		packs = new PackListPane();
		playerPane = new PlayerPane();

		bottom.getChildren().addAll(info, playerPane);
		contentLogin.getChildren().addAll(web, bottom);
		horizontal.getChildren().addAll(contentLogin, packs);
		return horizontal;
	}

	public PackInfoPane getInfo() {
		return info;
	}

	public Stage getStage() {
		return stage;
	}

	public PackListPane getPacks() {
		return packs;
	}

	public static Launcher getInstance() {
		return instance;
	}

	public PlayerPane getPlayerPane() {
		return playerPane;
	}
}
