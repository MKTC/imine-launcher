package nl.makertim.iminelauncher;

public class Version implements Comparable<Version> {

	private String version;

	public Version(int... ints) {
		this(getVersion(ints));
	}

	public Version(String version) {
		if (version == null) {
			throw new IllegalArgumentException("Version can not be null");
		}
		if (!version.matches("[0-9a-zA-Z]+(\\.[0-9a-zA-Z]+)*")) {
			throw new IllegalArgumentException("Invalid version format");
		}
		StringBuilder sb = new StringBuilder();
		for (char c : version.toCharArray()) {
			if (Character.isAlphabetic(c)) {
				String nmr = Integer.toString((Character.toLowerCase(c) - 'a'));
				while (nmr.length() < 2) {
					nmr = 0 + nmr;
				}
				sb.append(nmr);
				continue;
			}
			sb.append(c);
		}
		this.version = sb.toString();
	}

	public final String get() {
		return this.version;
	}

	@Override
	public int compareTo(Version that) {
		if (that == null) {
			return 1;
		}
		String[] thisParts = this.get().split("\\.");
		String[] thatParts = that.get().split("\\.");
		int length = Math.max(thisParts.length, thatParts.length);
		for (int i = 0; i < length; i++) {
			int thisPart = i < thisParts.length ? Integer.parseInt(thisParts[i]) : 0;
			int thatPart = i < thatParts.length ? Integer.parseInt(thatParts[i]) : 0;
			if (thisPart < thatPart) {
				return -1;
			}
			if (thisPart > thatPart) {
				return 1;
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object that) {
		return this == that || that != null && that instanceof Version && this.compareTo((Version) that) == 0;
	}

	@Override
	public String toString() {
		return version;
	}

	private static String getVersion(int... ints) {
		StringBuilder builder = new StringBuilder();
		for (int i : ints) {
			builder.append(i);
			builder.append(".");
		}
		return builder.substring(0, builder.length() - 1);
	}

}