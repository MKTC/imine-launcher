package nl.makertim.iminelauncher.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.OS;
import org.apache.commons.io.FileUtils;

public class FileUtil {

	private static final String NAME_IMINE = "iminelauncher";
	private static final String DIR_NAME_IMINE = "." + NAME_IMINE + "/";

	private static final String NAME_MOJANG = "minecraft";
	private static final String DIR_NAME_MOJANG = "." + NAME_MOJANG + "/";

	private static File location;

	public static void setLocation(File location) {
		FileUtil.location = location;
	}

	public static File getMojangApplicationDataFolder() {
		return getWorkingDir(DIR_NAME_MOJANG, NAME_MOJANG);
	}


	public static File getApplicationDataFolder() {
		if (location == null) {
			File workingDirectory = getWorkingDir(DIR_NAME_IMINE, NAME_IMINE);
			File fileLocationFile = new File(workingDirectory, "filelocation.txt");
			if (!fileLocationFile.exists()) {
				try {
					FileUtils.writeStringToFile(fileLocationFile, workingDirectory.getAbsolutePath(),
						Charset.defaultCharset());
				} catch (IOException ex) {
					Logger.getLogger().error("Error while writing filelocation.txt", ex);
				}
			}
			try {
				location = new File(FileUtils.readFileToString(fileLocationFile, Charset.defaultCharset()));
				location.mkdirs();
			} catch (IOException ex) {
				location = workingDirectory;
				Logger.getLogger().error("Error while reading filelocation.txt", ex);
			}
		}
		return location;
	}

	private static File getWorkingDir(String dirname, String name) {
		String userHome = System.getProperty("user.home", ".");
		switch (OS.getOS()) {
		case LINUX:
			return new File(userHome, dirname);
		case MAC:
			return new File(userHome, "Library/Application Support/" + name);
		case WINDOWS:
			String applicationData = System.getenv("APPDATA");
			String folder = applicationData != null ? applicationData : userHome;
			return new File(folder, dirname);
		default:
		case OTHER:
			return new File(userHome, name + "/");
		}
	}

	// http://www.avajava.com/tutorials/lessons/how-do-i-unzip-the-contents-of-a-zip-file.html
	public static void unzip(File zip) {
		try {
			ZipFile zipFile = new ZipFile(zip);
			Enumeration<? extends ZipEntry> enu = zipFile.entries();
			while (enu.hasMoreElements()) {
				ZipEntry zipEntry = enu.nextElement();

				String name = zipEntry.getName();
				long size = zipEntry.getSize();
				long compressedSize = zipEntry.getCompressedSize();

				File file = new File(zip.getParentFile(), name);
				if (name.endsWith("/")) {
					file.mkdirs();
					continue;
				}

				File parent = file.getParentFile();
				if (parent != null) {
					parent.mkdirs();
				}

				InputStream is = zipFile.getInputStream(zipEntry);
				FileOutputStream fos = new FileOutputStream(file);
				byte[] bytes = new byte[1024];
				int length;
				while ((length = is.read(bytes)) >= 0) {
					fos.write(bytes, 0, length);
				}
				is.close();
				fos.close();
			}
			zipFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
