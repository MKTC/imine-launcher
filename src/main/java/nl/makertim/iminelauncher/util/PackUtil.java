package nl.makertim.iminelauncher.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import javafx.scene.control.Alert;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.PackDownloader;
import nl.makertim.iminelauncher.mojang.BootstrapStarter;
import nl.makertim.iminelauncher.mojang.LauncherProfileInjector;
import nl.makertim.iminelauncher.pack.Pack;

public class PackUtil {

	public static final List<Consumer<Pack>> STATUS_UPDATERS = new ArrayList<>();

	public static void updatePack(Pack pack, Consumer<Double> progress) {
		ScheduleUtil.setWorking();
		pack.backup();
		try {
			PackDownloader.installPack(pack, progress);
		} catch (Exception ex) {
			try {
				Alert alarm = new Alert(Alert.AlertType.ERROR);
				alarm.setHeaderText(ex.getMessage());
				alarm.show();
			} catch (Throwable thr) {
				Logger.getLogger().error("Alarm ging niet af " + ex.getMessage(), thr);
			}

		}
		updatePackStatus(pack);
		ScheduleUtil.setDone();
	}

	public static void startMinecraft(Pack pack) {
		LauncherProfileInjector lpi = new LauncherProfileInjector(pack);
		lpi.inject();
		lpi.selectPack();
		BootstrapStarter.startMinecraft();
	}

	public static void injectPack(Pack pack) {
		try {
			Thread.sleep(2000L);
		} catch (Exception ex) {
		}
		Logger.getLogger().debug("Injecting into minecraft launcher...");
		LauncherProfileInjector lpi = new LauncherProfileInjector(pack);
		lpi.inject();
		lpi.selectPack();
	}

	public static void updatePackStatus(Pack pack) {

	}

}
