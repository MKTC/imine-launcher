package nl.makertim.iminelauncher.util;

public class ScheduleUtil {

	private static boolean isWorking = false;

	public static void setDone() {
		ScheduleUtil.isWorking = false;
	}

	public static void setWorking() {
		ScheduleUtil.isWorking = true;
	}

	public static boolean isWorking() {
		return isWorking;
	}
}
