package nl.makertim.iminelauncher.util;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.io.FileUtils;

import nl.makertim.iminelauncher.Logger;

/**
 * @author Tim Biesenbeek
 */
public class ImgUtil {

	public static File getCachedImg(String url) {
		File file = new File(FileUtil.getApplicationDataFolder(),
				new String(Base64.getEncoder().encode(url.getBytes())) + ".png");
		if (!file.exists()) {
			try {
				Logger.getLogger().debug("Downloading image from " + url);
				FileUtils.writeByteArrayToFile(file, WebUtil.getFile(url));
			} catch (IOException ex) {
				Logger.getLogger().error("Downloading image", ex);
			}
		}
		return file;
	}

	public static BufferedImage resizeImg(BufferedImage original, int size) {
		return resizeImg(original, size, size);
	}

	public static BufferedImage resizeImg(BufferedImage original, int width, int height) {
		BufferedImage resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resizedImage.createGraphics();
		int newHeight = Math.min(height, width / original.getWidth() * original.getHeight());

		g2d.drawImage(original, 0, 0, width, newHeight, null);
		g2d.dispose();

		return resizedImage;
	}
}
