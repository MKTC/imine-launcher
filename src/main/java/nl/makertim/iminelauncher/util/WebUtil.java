package nl.makertim.iminelauncher.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class WebUtil {

	private final static HttpClient CLIENT = HttpClientBuilder.create().build();

	public static String pastebin(String toPastebin) {
		String ret = null;
		try {
			ret = getPost(new URL("http://pastebin.com/api/api_post.php"),
				"api_dev_key=72bce20d718545786471dd53138852e0&api_option=paste&api_paste_code=" + toPastebin);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	public static String getDomain(String url, boolean incSub) {
		String reg = incSub
				? "((?:\\w{0,}\\.)*(?:\\w[-\\w]{0,61}\\w|\\w)(?:\\.\\w[-\\w]{0,61}\\w|\\w)*?\\.(?:\\w{2,3}\\.)?\\w+)"
				: "(?:\\w{0,}\\.)*((?:\\w[-\\w]{0,61}\\w|\\w)(?:\\.\\w[-\\w]{0,61}\\w|\\w)*?\\.(?:\\w{2,3}\\.)?\\w+)";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(url);
		if (m.find()) {
			return m.group(1);
		}
		return url;
	}

	public static String getResponse(String url) {
		String ret = null;
		try {
			ret = getResponse(new URL(url));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	public static String getResponse(URL url) {
		return getResponseWith(url, "");
	}

	public static String getResponseWith(URL url, char newLineChar) {
		return getResponseWith(url, Character.toString(newLineChar));
	}

	public static String getResponseWith(URL url, String newLineString) {
		String ret = "";
		for (String responseLine : getResponseLines(url)) {
			ret += responseLine + newLineString;
		}
		return ret;
	}

	public static byte[] getFile(String url) {
		List<Integer> response = new ArrayList<>();
		try {
			HttpRequestBase request = new HttpGet(url);
			request.addHeader("User-Agent", "Mozilla/5.0");

			HttpResponse httpResponse = CLIENT.execute(request);

			InputStream is = httpResponse.getEntity().getContent();
			int inByte;
			while ((inByte = is.read()) != -1)
				response.add(inByte);
			is.close();
		} catch (UnknownHostException ex) {
			System.err.println(ex);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		byte[] returns = new byte[response.size()];
		for (int i = 0; i < response.size(); i++) {
			returns[i] = (byte) ((int) response.get(i));
		}
		return returns;
	}

	private static String[] doRequest(HttpRequestBase request, String postData) {
		List<String> response = new ArrayList<>();
		try {
			request.addHeader("User-Agent", "Mozilla/5.0");
			if (request instanceof HttpEntityEnclosingRequestBase && postData != null && !postData.isEmpty()) {
				HttpEntityEnclosingRequestBase entityRequest = (HttpEntityEnclosingRequestBase) request;
				entityRequest.setEntity(new StringEntity(postData));
			}

			HttpResponse httpResponse = CLIENT.execute(request);

			Reader reader = new InputStreamReader(httpResponse.getEntity().getContent());
			BufferedReader rd = new BufferedReader(reader);

			String line;
			while ((line = rd.readLine()) != null) {
				response.add(line);
			}
			rd.close();
			reader.close();
		} catch (UnknownHostException ex) {
			System.err.println(ex);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return response.toArray(new String[response.size()]);
	}

	public static String[] getResponseLines(URL url) {
		return doRequest(new HttpGet(url.toString()), null);
	}

	public static String getPost(URL url, String post) {
		return getResponseWith(url, "");
	}

	public static String getPostWith(URL url, String post, char newLineChar) {
		return getPostWith(url, post, Character.toString(newLineChar));
	}

	public static String getPostWith(URL url, String post, String newLineString) {
		String ret = "";
		for (String responseLine : getPostLines(url, post)) {
			ret += responseLine + newLineString;
		}
		return ret;
	}

	public static String[] getPostLines(URL url, String post) {
		return doRequest(new HttpPost(url.toString()), post);
	}

	public static String postWithType(URL url, String post, String contentType) throws IOException {
		HttpURLConnection connection = createUrlConnection(url);
		byte[] postAsBytes = post.getBytes("UTF-8");
		connection.setRequestProperty("Content-Type", contentType + "; charset=utf-8");
		connection.setRequestProperty("Content-Length", "" + postAsBytes.length);
		connection.setDoOutput(true);
		OutputStream outputStream = null;
		try {
			outputStream = connection.getOutputStream();
			IOUtils.write(postAsBytes, outputStream);
		} finally {
			IOUtils.closeQuietly(outputStream);
		}
		InputStream inputStream = null;
		try {
			inputStream = connection.getInputStream();
			String result = IOUtils.toString(inputStream, "UTF-8");
			return result;
		} catch (IOException e) {
			IOUtils.closeQuietly(inputStream);
			inputStream = connection.getErrorStream();

			if (inputStream != null) {
				String result = IOUtils.toString(inputStream, "UTF-8");
				return result;
			}
			throw e;
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
	}

	private static HttpURLConnection createUrlConnection(URL url) throws IOException {
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(15000);
		connection.setReadTimeout(15000);
		connection.setUseCaches(false);
		return connection;
	}
}
