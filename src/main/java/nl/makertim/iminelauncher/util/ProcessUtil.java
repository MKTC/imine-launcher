package nl.makertim.iminelauncher.util;

import nl.makertim.iminelauncher.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public class ProcessUtil {

	public static void startProcess(File dir, Consumer<String> inputStream, Consumer<String> errorStream, String... args) throws IOException {
		Process process = new ProcessBuilder(args).directory(dir).start();
		BufferedReader brIn = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader brErr = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		registerReader(inputStream, brIn);
		registerReader(errorStream, brErr);
	}

	private static void registerReader(Consumer<String> stream, BufferedReader reader) {
		new Thread(() -> {
			String line;
			try {
				while ((line = reader.readLine()) != null) {
					stream.accept(line);
				}
			} catch (Exception ex) {
				Logger.getLogger().debug("Consumer fault:", ex);
			}
		}).start();
	}
}
