package nl.makertim.iminelauncher.prelauncher;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.spi.LoggingEvent;

import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.Main;

public class Console extends Frame {

	public static Console INSTANCE;
	private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

	private List console;
	private Button pastebinButton;
	private StringBuilder consoleBackup = new StringBuilder();

	public Console() {
		INSTANCE = this;
		Logger.getLogger().debug("Starting console");
		setupLayout();
		registerEvents();
		Logger.getLogger().debug("Showing console");
		setVisible(true);
		registerToLogger();
	}

	private void registerToLogger() {
		Logger.getLogger().debug("Injecting logger");
		Appender appender = new ConsoleAppender() {
			@Override
			public void append(LoggingEvent event) {
				Object message = event.getMessage();
				if (message == null) {
					message = "null";
				}
				if (!message.toString().startsWith("[")) {
					message = String.format("[%s %s]: \t%s", sdf.format(new Date()), event.getLevel(), message);
				}
				addToConsole(message);
			}
		};
		appender.setName("Console Tap");
		Logger.getLogger().addAppender(appender);
	}

	public void addToConsole(Object obj) {
		if (obj == null) {
			obj = "null";
		}
		if (isVisible()) {
			console.add(obj.toString());
			console.select(console.getItemCount() - 1);
		}
		consoleBackup.append(obj).append("\n");
	}

	private void setupLayout() {
		BorderLayout bl = new BorderLayout();
		setSize(800, 300);
		setResizable(true);
		setTitle("Console");

		try {
			URL icon = getClass().getResource("iMineIcon.png");
			if (icon == null) {
				icon = getClass().getResource("/iMineIcon.png");
			}
			setIconImage(ImageIO.read(icon));
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		console = new List();
		addToConsole("Console: " + Main.VERSION);
		console.setForeground(Color.white);
		console.setFont(Font.getFont("Arial"));
		console.setBackground(new Color(28, 28, 28));
		pastebinButton = new Button("Copy log");
		pastebinButton.setLocation(0, 0);

		bl.setHgap(0);
		bl.setVgap(0);
		setLayout(bl);
		add(console, BorderLayout.CENTER);
		add(pastebinButton, BorderLayout.SOUTH);
	}

	private void registerEvents() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				dispose();
			}
		});
		pastebinButton.addActionListener(e -> {
			Logger.getLogger().debug("Copy to clipboard");
			pastebinButton.setEnabled(false);
			StringSelection stringSelection = new StringSelection(consoleBackup.toString());
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(stringSelection, null);
			Dialog dialog = new Dialog(Console.this, "Copied to clipboard!");
			Point parent1 = Console.this.getLocation();
			dialog.setLocation(parent1.x + 100, parent1.y + 100);
			dialog.setSize(400, 200);
			dialog.setVisible(true);
			dialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					dialog.dispose();
				}
			});
			new Thread(() -> {
				try {
					Thread.sleep(2000L);
				} catch (InterruptedException ex) {
				}
				pastebinButton.setEnabled(true);
				dialog.dispose();
			}).run();
		});
	}

	public void showDownloadButton() {
		Button updateButton = new Button("Download newest version");
		updateButton.setBackground(Color.yellow);
		updateButton.addActionListener(e -> {
			updateButton.setEnabled(false);
			try {
				Desktop.getDesktop().browse(new URI("http://files.imine.nl/iMine/iMineLauncher.jar"));
			} catch (Exception ex) {
				Logger.getLogger().error("Couldn't start browser :(", ex);
			}
			updateButton.setEnabled(true);
		});
		add(updateButton, BorderLayout.NORTH);
		setBounds(getX(), getY(), getWidth(), getHeight() + 20);
	}

}
