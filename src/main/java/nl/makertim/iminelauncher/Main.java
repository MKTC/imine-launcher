package nl.makertim.iminelauncher;

import java.lang.management.ManagementFactory;

import org.apache.log4j.Level;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import nl.makertim.iminelauncher.prelauncher.Console;

public class Main {

	public static final Version VERSION = new Version(0, 3, 3);
	public static final int RAM_MB = (int) Math
			.round(((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
					.getTotalPhysicalMemorySize() / 1024D / 1024D);
	public static final int RAM_MB_SPARE = (int) Math
			.round(((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
					.getFreePhysicalMemorySize() / 1024D / 1024D);
	public static final int RAM_GB = (int) Math.round(RAM_MB / 1024D);
	public static final int RAM_GB_SPARE = (int) Math.round(RAM_MB_SPARE / 1024D);

	public static void main(String[] args) {
		start(args);
		new Console();
		Logger.getLogger().info("Starting launcher!");
		try {
			javafx.application.Application.launch(Launcher.class, args);
		} catch (Throwable thr) {
			Logger.getLogger().error("Application crash?! " + thr, thr);
		}
	}

	private static void start(String[] args) {
		OptionParser parser = new OptionParser();
		parser.allowsUnrecognizedOptions();

		OptionSpec logLevel = parser.accepts("logger").withRequiredArg().defaultsTo("INFO").ofType(LogLevel.class);
		OptionSpec nonOptions = parser.nonOptions();
		OptionSet optionSet = parser.parse(args);
		for (Object nonUsedArg : optionSet.valuesOf(nonOptions)) {
			System.out.println(nonUsedArg + " is not a valid argument");
		}
		try {
			Class<?> levelClass = Level.class;
			Level level = (Level) levelClass
					.getDeclaredField((LogLevel.valueOf(optionSet.valueOf(logLevel).toString().toUpperCase()).name()))
					.get(null);
			Logger.getLogger().setLevel(level);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
