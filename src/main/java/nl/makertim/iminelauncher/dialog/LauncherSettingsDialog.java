package nl.makertim.iminelauncher.dialog;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import nl.makertim.iminelauncher.util.FileUtil;
import nl.makertim.iminelauncher.Launcher;
import nl.makertim.iminelauncher.Logger;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.charset.Charset;
import java.util.Optional;

public class LauncherSettingsDialog extends Dialog {

	private TextField location;

	public LauncherSettingsDialog() {
		setWidth(300);
		setup();
		open();
	}

	private void setup() {
		DialogPane pane = new DialogPane();
		pane.setHeaderText("Settings");
		VBox items = new VBox(8);

		VBox ramBox = new VBox(0);
		HBox ramRow1 = new HBox(0);
		Label lblFree = new Label("Ram free: ");
		Label ramFree = new Label("0MB");

		HBox ramRow2 = new HBox(0);
		Label lblRam = new Label("Ram usage: ");
		Label ramUse = new Label("0MB");
		Label lblSpacer = new Label(" / ");
		Label ramTotal = new Label("0MB");

		new Thread(() -> {
			do {
				int freeRam = (int) Math
						.round(((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
								.getFreePhysicalMemorySize() / 1024D / 1024D);
				int totalRam = (int) Math
						.round(((com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean())
								.getTotalPhysicalMemorySize() / 1024D / 1024D);
				Platform.runLater(() -> {
					ramFree.setText(freeRam + "MB");
					ramUse.setText((totalRam - freeRam) + "MB");
					ramTotal.setText(totalRam + "MB");
				});
				try {
					Thread.sleep(500);
				} catch (Exception ex) {
				}
			} while (isShowing());
		}).start();
		ramBox.getChildren().addAll(ramRow1, ramRow2);
		ramRow1.getChildren().addAll(lblFree, ramFree);
		ramRow2.getChildren().addAll(lblRam, ramUse, lblSpacer, ramTotal);
		items.getChildren().add(ramBox);

		try {
			VBox locationBox = new VBox(0);
			Label lblLocation = new Label("Install location: ");
			location = new TextField(FileUtils.readFileToString(
				new File(FileUtil.getApplicationDataFolder(), "filelocation.txt"), Charset.defaultCharset()));
			location.setEditable(false);
			Button browse = new Button("Browse...");
			browse.setOnMouseClicked(click -> {
				DirectoryChooser chooser = new DirectoryChooser();
				chooser.setInitialDirectory(new File(location.getText()));
				File newLocation = chooser.showDialog(getOwner());
				if (newLocation != null) {
					location.setText(newLocation.getAbsolutePath());
					FileUtil.setLocation(newLocation);
					Launcher.getInstance().getInfo().updatePackStatus();
				}
			});
			locationBox.getChildren().addAll(lblLocation, location, browse);
			items.getChildren().addAll(locationBox);
		} catch (IOException e) {
			e.printStackTrace();
		}

		pane.getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.APPLY);
		pane.setContent(items);
		setDialogPane(pane);

		((Stage) getDialogPane().getScene().getWindow()).getIcons().add(new Image("iMineIcon.png"));
	}

	private void open() {
		Optional<ButtonType> result = showAndWait();
		if (result.isPresent()) {
			if (result.get() == ButtonType.APPLY) {
				try {
					FileUtils.write(new File(FileUtil.getApplicationDataFolder(), "filelocation.txt"),
						location.getText(), Charset.defaultCharset());
				} catch (IOException ex) {
					Logger.getLogger().error("Filelocation not saved", ex);
				}
				Logger.getLogger().info("Saved new config");
			}
		}
	}

}
