package nl.makertim.iminelauncher.dialog;

import java.awt.*;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.revwalk.RevCommit;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.Main;
import nl.makertim.iminelauncher.pack.Pack;

public class PackInfoDialog extends Dialog {

	public enum PopupType {
		INFO, COMMITS
	}

	private static final SimpleDateFormat sdf = new SimpleDateFormat("d/M/YYYY HH:mm");

	private Pack pack;

	public PackInfoDialog(PopupType type, Pack pack) {
		this.pack = pack;
		switch (type) {
		case INFO:
			info();
			break;
		case COMMITS:
			commits();
			break;
		}
		((Stage) getDialogPane().getScene().getWindow()).getIcons().add(new Image("iMineIcon.png"));
	}

	private void commits() {
		DialogPane pane = new DialogPane();
		pane.setHeaderText(pack.getName() + " commits");

		VBox list = new VBox(8);

		VBox versionBox = new VBox(0);
		Label versionYour = new Label("Your version: " + pack.getLocalVersion().toString());
		Label versionRemote = new Label("Newest version: " + pack.getNewVersion().toString());
		versionBox.getChildren().addAll(versionYour, versionRemote);

		VBox commits = new VBox(0);
		commits.getChildren().add(new Label("Last changes:"));
		lastCommits(commits, 25);

		list.getChildren().addAll(versionBox, commits);

		pane.getButtonTypes().add(ButtonType.CLOSE);
		pane.setContent(list);
		setDialogPane(pane);
	}

	private void info() {
		DialogPane pane = new DialogPane();
		pane.setHeaderText(pack.getName() + " info");

		VBox list = new VBox(8);

		VBox versionBox = new VBox(0);
		Label versionYour = new Label("Your version: " + pack.getLocalVersion());
		Label versionRemote = new Label("Newest version: " + pack.getNewVersion());
		versionBox.getChildren().addAll(versionYour, versionRemote);

		VBox commits = new VBox(0);
		commits.getChildren().add(new Label("Last changes:"));
		lastCommits(commits, 5);

		VBox ramBox = new VBox(0);
		Label minRam = new Label("Min. ram: " + pack.getMinRamSize() + "B");
		Label prefRam = new Label("Pref. ram: " + pack.getPrefRamSize() + "B");
		Label freeRam = new Label("Avg. free ram: " + Main.RAM_GB_SPARE + "GB  (" + Main.RAM_MB_SPARE + "MB )");
		Label yourRam = new Label("Your ram: " + Main.RAM_GB + "GB  (" + Main.RAM_MB + "MB )");
		if (pack.hasPrefRam()) {
			yourRam.setTextFill(Color.GREEN);
			freeRam.setTextFill(Color.GREEN);
		} else if (pack.hasEnoughRam()) {
			yourRam.setTextFill(Color.ORANGE);
			freeRam.setTextFill(Color.ORANGE);
		} else {
			yourRam.setTextFill(Color.RED);
			freeRam.setTextFill(Color.RED);
		}
		ramBox.getChildren().addAll(minRam, prefRam, freeRam, yourRam);

		VBox source = new VBox(0);
		Label sourceTxt = new Label("Source pack:");
		Hyperlink gitURL = new Hyperlink(pack.getUserUrl());
		gitURL.setOnMouseClicked(click -> {
			try {
				Desktop.getDesktop().browse(new URI(gitURL.getText()));
			} catch (Exception ex) {
				Logger.getLogger().error("Couldn't start browser :(", ex);
			}
		});
		source.getChildren().addAll(sourceTxt, gitURL);

		list.getChildren().addAll(versionBox, commits, ramBox, source);

		pane.getButtonTypes().add(ButtonType.CLOSE);
		pane.setContent(list);
		setDialogPane(pane);
	}

	private void lastCommits(VBox commits, int commitCount) {
		new Thread(() -> {
			try {
				Git git = Git.open(pack.getDirectory());
				git.fetch();
				LogCommand log = git.log();
				log.setMaxCount(commitCount);
				List<String> retList = new ArrayList<>();
				for (RevCommit version : log.call()) {
					Platform.runLater(() -> {
						commits.getChildren().add(new Label(String.format("  [%s] %s",
							sdf.format(version.getAuthorIdent().getWhen()), version.getShortMessage())));
						getDialogPane().getScene().getWindow().sizeToScene();
					});
				}
			} catch (Exception ex) {
				Logger.getLogger().error("Couldn't get last update message");
				Platform.runLater(() -> {
					commits.getChildren().add(new Label("unknown"));
					getDialogPane().getScene().getWindow().sizeToScene();
				});
			}
		}).start();
	}

}
