package nl.makertim.iminelauncher.dialog;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.io.FileUtils;

import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import nl.makertim.iminelauncher.util.FileUtil;
import nl.makertim.iminelauncher.Launcher;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.PackSelectionManager;

public class RepoManagementDialog extends Dialog {

	private VBox items;
	private ListView<String> repo;
	private TextField newRepo;

	public RepoManagementDialog() {
		setWidth(300);
		setup();
		open();
	}

	private void setup() {
		DialogPane pane = new DialogPane();
		pane.setHeaderText("Repositories");
		items = new VBox(8);

		buildListView();

		newRepo = new TextField();
		newRepo.setPromptText("http://git.imine.nl/imine/Launcher/raw/master/webfiles/packs.json");
		Button add = new Button("Add");
		add.setOnMouseClicked(click -> {
			if (!newRepo.getText().isEmpty()) {
				Logger.getLogger().info("Adding repo: " + newRepo.getText());
				try {
					new URL(newRepo.getText());
				} catch (MalformedURLException ex) {
					Logger.getLogger().warn("Repo not added because: " + ex.getLocalizedMessage(), ex);
					return;
				}
				repo.getItems().add(newRepo.getText());
			}
		});
		items.getChildren().addAll(newRepo, add);

		pane.getButtonTypes().addAll(ButtonType.CLOSE);
		pane.setContent(items);
		setDialogPane(pane);

		((Stage) getDialogPane().getScene().getWindow()).getIcons().add(new Image("iMineIcon.png"));
	}

	private void save() {
		try {
			StringBuilder builder = new StringBuilder();
			for (Object url : repo.getItems()) {
				builder.append(url).append("\n");
			}
			FileUtils.write(new File(FileUtil.getApplicationDataFolder(), "repos.txt"), builder.toString(),
				Charset.defaultCharset());
		} catch (Exception ex) {
			Logger.getLogger().error("Error while writing repo", ex);
		}
	}

	private void buildListView() {
		if (repo != null) {
			items.getChildren().remove(repo);
		}
		repo = new ListView<>();
		try {
			repo.getItems().addAll(FileUtils.readLines(new File(FileUtil.getApplicationDataFolder(), "repos.txt"),
				Charset.defaultCharset()));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		repo.setOnMouseClicked(click -> {
			if (click.getClickCount() > 1 && repo.getSelectionModel().getSelectedItem() != null) {
				repo.getItems().remove(repo.getSelectionModel().getSelectedItem());
				if (repo.getItems().size() == 0) {
					repo.getItems().add(PackSelectionManager.JSON);
				}
			}
		});
		items.getChildren().add(0, repo);
	}

	private void open() {
		showAndWait();
		save();
		PackSelectionManager manager = PackSelectionManager.getInstance();
		manager.getPacks().clear();
		manager.loadUrls();
		manager.addDefaults();
		manager.autoSelectLatest();
		Launcher.getInstance().getPacks().updatePackList();
		Logger.getLogger().info("Reloaded packs");
	}

}
