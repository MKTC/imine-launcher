package nl.makertim.iminelauncher;

import org.apache.log4j.Level;

public class Logger {

	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger("Launcher");

	public static org.apache.log4j.Logger getLogger() {
		return LOGGER;
	}

	static {
		LOGGER.setLevel(Level.INFO);
		LOGGER.info("OS name:\t" + System.getProperty("os.name"));
		LOGGER.info("OS version:\t" + System.getProperty("os.version"));
		LOGGER.info("OS type:\t" + System.getProperty("os.arch"));
		LOGGER.info("OS found:\t" + OS.getOS());
		LOGGER.info("Java version:\t" + System.getProperty("java.version"));
		LOGGER.info("Ram:\t" + Main.RAM_GB + "GB");
	}

}
