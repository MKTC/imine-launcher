package nl.makertim.iminelauncher.mojang;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import nl.makertim.iminelauncher.*;
import nl.makertim.iminelauncher.util.FileUtil;
import nl.makertim.iminelauncher.util.ProcessUtil;
import org.apache.commons.io.FileUtils;

public class BootstrapStarter {

	public static void startMinecraft() {
		Logger.getLogger().info("Starting minecraft");
		File launcherFile = new File(FileUtil.getApplicationDataFolder(), "launcher.jar");
		if (!launcherFile.exists()) {
			try {
				FileUtils.copyURLToFile(new URL("http://s3.amazonaws.com/Minecraft.Download/launcher/Minecraft.jar"),
					launcherFile);
			} catch (IOException ex) {
				Logger.getLogger().error("Downloading launcher error", ex);
			}
		}
		try {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ex) {
			}
			ProcessUtil.startProcess(FileUtil.getApplicationDataFolder(), in -> {
				Logger.getLogger().info(in);
				if (in.contains(": Goodbye.")) {
					Launcher.getInstance().syncSettings();
				}
			},
				err -> Logger.getLogger().warn(err), "java", "-jar", "launcher.jar", "-d64");
		} catch (IOException ex) {
			Logger.getLogger().error("Couldn't start launcher", ex);
		}
	}

}
