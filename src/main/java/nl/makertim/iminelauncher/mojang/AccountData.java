package nl.makertim.iminelauncher.mojang;

import java.util.UUID;

public class AccountData {

	private final String key;
	private final String displayName;
	private final UUID uuid;
	private final String username;

	public AccountData(String randomKeyThatNoOneUnderstands, String displayName, UUID uuid, String username) {
		this.key = randomKeyThatNoOneUnderstands;
		this.displayName = displayName;
		this.uuid = uuid;
		this.username = username;
	}

	public String getKey() {
		return key;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getUsername() {
		return username;
	}

	public UUID getUuid() {
		return uuid;
	}
}
