package nl.makertim.iminelauncher.mojang;

import static nl.makertim.iminelauncher.mojang.LauncherProfile.getRoot;

import java.util.*;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import nl.makertim.iminelauncher.util.StringUtil;

// This is the scariest class i have, plz if you don't trust me look good through this class
// In theory you could parse a token out of your file and do horrible stuff with it...
// If you say => Hey don't do this with my profile data, you can always say that to me
// I promise, the only things i do with it; are used only for the launcher!

public class LauncherProfileGetter {

	public static String getSelectedProfile() {
		JsonObject json = getRoot();
		// TODO: selected pack
		if (json.has("selectedProfile")) {
			return json.get("selectedProfile").getAsString();
		}
		return "";
	}

	public static String getSelectedAccount() {
		JsonObject root = getRoot();
		if (root.has("selectedUser")) {
			if (root.get("selectedUser").isJsonPrimitive()) {
				return root.get("selectedUser").getAsString();
			}
			JsonObject selectedUser = root.get("selectedUser").getAsJsonObject();
			if (selectedUser.has("account")) {
				String userKey = selectedUser.get("account").getAsString();
				if (root.has("authenticationDatabase")) {
					JsonObject authenticationDatabase = root.get("authenticationDatabase").getAsJsonObject();
					if (authenticationDatabase.has(userKey)) {
						JsonObject userData = authenticationDatabase.get(userKey).getAsJsonObject();
						return userData.get("username").getAsString();
					}
				}
			}
		}
		return "";
	}

	public static List<AccountData> getAccounts() {
		List<AccountData> accountData = new ArrayList<>();
		JsonObject json = getRoot();
		if (json.has("authenticationDatabase")) {
			JsonObject authenticationDatabase = json.getAsJsonObject("authenticationDatabase");
			for (Map.Entry<String, JsonElement> accountEntry : authenticationDatabase.entrySet()) {
				if (accountEntry.getValue().isJsonObject()) {
					JsonObject account = accountEntry.getValue().getAsJsonObject();
					String displayName = "unknown";
					UUID uuid = UUID.fromString("00000000-0000-0000-0000-000000000000");
					String username = account.has("username") ? account.get("username").getAsString() : "unknown";
					if (account.has("uuid") || account.has("displayName")) {
						if (account.has("uuid")) {
							uuid = UUID.fromString(account.get("uuid").getAsString());
						}
						if (account.has("displayName")) {
							displayName = account.get("displayName").getAsString();
						}
					} else {
						if (account.has("profiles")) {
							Optional<Map.Entry<String, JsonElement>> optionalProfile = account.get("profiles")
									.getAsJsonObject().entrySet().stream().findFirst();
							if (optionalProfile.isPresent()) {
								Map.Entry<String, JsonElement> profile = optionalProfile.get();
								uuid = StringUtil.fromWebString(profile.getKey());
								displayName = profile.getValue().getAsJsonObject().get("displayName").getAsString();
							}
						}
					}
					accountData.add(new AccountData(accountEntry.getKey(), displayName, uuid, username));
				}
			}
		}
		return accountData;
	}
}
