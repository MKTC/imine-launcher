package nl.makertim.iminelauncher.mojang;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import nl.makertim.iminelauncher.util.FileUtil;
import nl.makertim.iminelauncher.Logger;

import java.io.File;
import java.io.FileReader;

public class LauncherProfile {
	static final File LAUNCHER_PROFILES;

	static {
		LAUNCHER_PROFILES = new File(FileUtil.getMojangApplicationDataFolder(), "launcher_profiles.json");
		if (!LAUNCHER_PROFILES.exists()) {
			Platform.runLater(() -> {
				Alert warn = new Alert(Alert.AlertType.WARNING);
				warn.setHeaderText("Minecraft has never run before, running minecraft...");
				warn.setContentText("You can close the launcher right after logging in.");
				warn.showAndWait();
			});
			BootstrapStarter.startMinecraft();
		}
	}

	public static JsonObject getRoot() {
		JsonObject json = new JsonObject();
		try {
			FileReader fr = new FileReader(LAUNCHER_PROFILES);
			json = new JsonParser().parse(fr).getAsJsonObject();
			fr.close();
		} catch (Exception ex) {
			Logger.getLogger().error("launcher profile not working", ex);
		}
		return json;
	}
}
