package nl.makertim.iminelauncher.mojang;

import static nl.makertim.iminelauncher.mojang.LauncherProfile.LAUNCHER_PROFILES;
import static nl.makertim.iminelauncher.mojang.LauncherProfile.getRoot;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.Main;
import nl.makertim.iminelauncher.pack.Forge;
import nl.makertim.iminelauncher.pack.Pack;

public class LauncherProfileInjector {

	private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	private final String type;
	private final String name;
	private final File packDir;
	private final String version;
	private final String args;
	private final boolean useHopper;
	private final String launcherVisibility;
	private final String icon;

	public LauncherProfileInjector(Pack pack) {
		this.type = pack.getClass().getSimpleName();
		this.name = pack.getName();
		this.icon = pack.getIcon();
		if (pack.isExternal()) {
			this.packDir = pack.getDirectory();
		} else {
			this.packDir = null;
		}
		if (pack instanceof Forge) {
			this.version = ((Forge) pack).getForgeVersion();
		} else {
			this.version = null;
		}
		if (pack.hasPrefRam()) {
			this.args = String.format(
				"-Xmx%1$s -Xms%1$s -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn128M",
				pack.getPrefRamSize());
		} else if (pack.hasEnoughRam()) {
			this.args = String.format(
				"-Xmx%1$s -Xms%1$s -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn128M",
				pack.getMinRamSize());
		} else {
			try {
				Alert alert = new Alert(AlertType.WARNING);
				alert.setHeaderText("Not enough ram detected!");
				alert.setContentText(
					String.format("Ram detected: %sGB \nRam needed: %s", Main.RAM_GB, pack.getMinRamSize()));
				alert.showAndWait();
			} catch (Throwable thr) {
				Logger.getLogger().error("alarm didnt trigger - ram tekort was de melding", thr);
				thr.printStackTrace();
			}
			this.args = "-XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn128M";
		}
		this.useHopper = pack.useHopper();
		this.launcherVisibility = "hide launcher and re-open when game closes";
	}

	public void inject() {
		Logger.getLogger().debug("Injecting pack into minecarft");
		try {
			JsonObject root = getRoot();
			JsonObject profiles = root.get("profiles").getAsJsonObject();
			String javaArgs = null;
			if (profiles.has(name)) {
				JsonElement jsonJavaArgs = profiles.get(name).getAsJsonObject().get("javaArgs");
				javaArgs = jsonJavaArgs == null ? null : jsonJavaArgs.getAsString();
				profiles.remove(name);
			}
			JsonObject packJson = new JsonObject();
			packJson.addProperty("name", name);
			packJson.addProperty("icon", icon);
			if (packDir != null) {
				packJson.addProperty("gameDir", packDir.getAbsolutePath());
			}
			if (version != null) {
				packJson.addProperty("lastVersionId", version);
			}
			if (args != null || javaArgs != null) {
				packJson.addProperty("javaArgs", javaArgs == null ? args : javaArgs);
			}
			if (!useHopper) {
				packJson.addProperty("useHopperCrashService", false);
			}
			packJson.addProperty("launcherVisibilityOnGameClose", launcherVisibility);
			profiles.add(name, packJson);
			Logger.getLogger().info(type + " added/updated: ");
			Logger.getLogger().info("  Name: " + packJson.get("name"));
			String ram = packJson.get("javaArgs").getAsString().replace("-Xmx", "");
			ram = ram.substring(0, ram.indexOf('M') == -1 ? ram.indexOf('G') + 1 : ram.indexOf('M') + 1) + "B";
			Logger.getLogger().info("  Ram: " + ram);
			Logger.getLogger().info("  Forge: " + version);
			Logger.getLogger().debug("Inserted: " + new GsonBuilder().create().toJson(packJson));

			FileWriter fw = new FileWriter(LAUNCHER_PROFILES);
			fw.write(root.toString());
			fw.flush();
			fw.close();
		} catch (Exception ex) {
			Logger.getLogger().error("Error while injecting modpack", ex);
		}
	}

	public void removePack() {
		try {
			JsonObject root = getRoot();
			JsonObject profiles = root.get("profiles").getAsJsonObject();
			if (profiles.has(name)) {
				profiles.remove(name);
			}
			Logger.getLogger().debug("Removed pack from launcher: " + name);

			FileWriter fw = new FileWriter(LAUNCHER_PROFILES);
			fw.write(root.toString());
			fw.flush();
			fw.close();
		} catch (Exception ex) {
			Logger.getLogger().error("Error while injecting modpack", ex);
		}
	}

	public void selectPack() {
		Logger.getLogger().debug("Select pack " + name);
		try {
			JsonObject root = getRoot();
			JsonObject profiles = root.get("profiles").getAsJsonObject();
			if (profiles.has(name)) {
				JsonObject packObj = profiles.get(name).getAsJsonObject();
				if (packObj.has("lastUsed")) {
					packObj.remove("lastUsed");
				}
				if (packObj.has("created")) {
					packObj.remove("created");
				}
				packObj.addProperty("created", SIMPLE_DATE_FORMAT.format(new Date()));
				packObj.addProperty("lastUsed", SIMPLE_DATE_FORMAT.format(new Date()));
			} else {
				Logger.getLogger().debug("No " + name + " found.");
			}

			if (root.has("selectedProfile")) {
				root.remove("selectedProfile");
			}
			root.addProperty("selectedProfile", name);

			FileWriter fw = new FileWriter(LAUNCHER_PROFILES);
			fw.write(root.toString());
			fw.flush();
			fw.close();
		} catch (Exception ex) {
			Logger.getLogger().error("Error while selectPack", ex);
		}
	}

	public static void selectUser(AccountData account) {
		Logger.getLogger().debug("Select user " + account.getDisplayName());
		try {
			JsonObject root = getRoot();
			boolean oldLauncher = root.get("selectedUser").isJsonPrimitive();
			if (root.has("selectedUser")) {
				root.remove("selectedUser");
			}
			if (oldLauncher) {
				root.addProperty("selectedUser", account.getKey());
			} else {
				JsonObject selectedUser = new JsonObject();
				selectedUser.addProperty("account", account.getKey());
				selectedUser.addProperty("profile", account.getUuid().toString().replaceAll("-", ""));
				root.add("selectedUser", selectedUser);
			}

			FileWriter fw = new FileWriter(LAUNCHER_PROFILES);
			fw.write(root.toString());
			fw.flush();
			fw.close();
		} catch (Exception ex) {
			Logger.getLogger().error("Error while selectPack", ex);
		}
	}
}
