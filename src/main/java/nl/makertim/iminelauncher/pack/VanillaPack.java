package nl.makertim.iminelauncher.pack;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;

import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.OS;
import nl.makertim.iminelauncher.Version;
import nl.makertim.iminelauncher.util.FileUtil;

public final class VanillaPack extends AbstractPack {

	public static final VanillaPack VANILLA_PACK = new VanillaPack();

	private VanillaPack() {
		super("vanilla", "Vanilla", "http://i.imgur.com/wYemiSu.png", "512M", "1024M", false, true);
	}

	@Override
	public Version getLocalVersion() {
		File[] versions = new File(FileUtil.getMojangApplicationDataFolder(), "versions").listFiles();
		List<Version> versionList = new ArrayList<>();
		for (File version : versions) {
			if (!version.isDirectory() || !version.getName().matches("[0-9]+(\\.[0-9]+)*")) {
				continue;
			}
			versionList.add(new Version(version.getName()));
		}
		versionList.sort((v1, v2) -> v2.compareTo(v1));
		return versionList.get(0);
	}

	@Override
	public Version getNewVersion() {
		return getLocalVersion();
	}

	@Override
	public String getUrl() {
		return "https://minecraft.net/";
	}

	@Override
	public PackStatus getStatus() {
		return PackStatus.UP_TO_DATE;
	}

	@Override
	public void installClient(File installLocation, Consumer<Double> statusUpdate) {
	}

	@Override
	public void removeClient(File from) {
		Logger.getLogger().info("Removing logs from vanilla");
		try {
			FileUtils.deleteDirectory(new File(FileUtil.getMojangApplicationDataFolder(), "logs"));
		} catch (IOException ex) {
			Logger.getLogger().error("Couldn't clean forge installation", ex);
		}
	}

	@Override
	public void installServer(File installLocation, Consumer<Double> statusUpdate) {
		try {
			String version = getNewVersion().toString();
			FileUtils.copyURLToFile(new URL("https://s3.amazonaws.com/Minecraft.Download/versions/" + version
					+ "/minecraft_server." + version + ".jar"),
				new File(installLocation, "minecraft_server." + version + ".jar"));
		} catch (Exception ex) {
			Logger.getLogger().error("Server downloading went wrong.", ex);
		}
		OS.getOS().openFolder(installLocation);
	}
}
