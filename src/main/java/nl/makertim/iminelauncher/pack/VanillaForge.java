package nl.makertim.iminelauncher.pack;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;

import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.Version;
import nl.makertim.iminelauncher.util.FileUtil;
import nl.makertim.iminelauncher.util.WebUtil;

public class VanillaForge extends AbstractPack implements Forge {

	public static final VanillaForge FORGE_PACK = new VanillaForge("forge", "Forge", "http://i.imgur.com/cHpf5eE.png",
			"512M", "1G", false);

	public VanillaForge(String id, String name, String imgURL, String minRamSize, String prefRamSize, boolean isExternal) {
		super(id, name, imgURL, minRamSize, prefRamSize, isExternal, false);
	}

	@Override
	public String getForgeVersion() {
		String latest = getLatestForge();
		latest = latest.split("-")[0] + "-forge" + latest;
		return latest;
	}

	@Override
	public Version getLocalVersion() {
		File[] versions = new File(FileUtil.getMojangApplicationDataFolder(), "versions").listFiles();
		List<Version> versionList = new ArrayList<>();
		for (File version : versions) {
			if (!version.isDirectory() || !version.getName().contains("forge")
					|| !version.getName().split("-")[2].matches("[0-9]+(\\.[0-9]+)*")) {
				continue;
			}
			versionList.add(new Version(version.getName().split("-")[2]));
		}
		versionList.sort((v1, v2) -> v2.compareTo(v1));
		if (versionList.size() == 0) {
			return new Version(0);
		}
		return versionList.get(0);
	}

	@Override
	public Version getNewVersion() {
		String latest = getLatestForge();
		if (latest.isEmpty()) {
			return new Version(0);
		}
		return new Version(latest.split("-")[1]);
	}

	@Override
	public String getUrl() {
		return "http://files.minecraftforge.net/";
	}

	@Override
	public PackStatus getStatus() {
		if (getLocalVersion().compareTo(new Version(0)) == 0) {
			return PackStatus.NEW;
		}
		Version current = getLocalVersion();
		int version = current.compareTo(getNewVersion());
		if (version < 0) {
			Logger.getLogger().info("New version forge available " + getNewVersion());
			return PackStatus.OTHER;
		}
		return PackStatus.UP_TO_DATE;
	}

	@Override
	public void installClient(File installLocation, Consumer<Double> statusUpdate) {
		Forge.super.installClient(installLocation, statusUpdate);
	}

	@Override
	public void removeClient(File from) {
		try {
			FileUtils.deleteDirectory(new File(getDirectory(), "mods"));
			FileUtils.deleteDirectory(new File(getDirectory(), "config"));
		} catch (IOException ex) {
			Logger.getLogger().error("Couldn't clean forge installation", ex);
		}
	}

	@Override
	public void installServer(File installLocation, Consumer<Double> statusUpdate) {
		preInstallServer(installLocation);
		postInstallServer(installLocation);
	}

	protected String getLatestForge() {
		try {
			String[] versionXml = WebUtil.getResponseLines(
				new URL("http://files.minecraftforge.net/maven/net/minecraftforge/forge/maven-metadata.xml"));
			String ret = null;
			for (String versionXmlLine : versionXml) {
				if (versionXmlLine.matches("\\s+<version>.+")) {
					ret = versionXmlLine.replaceAll("\\s+<version>", "").replaceAll("</version>", "").trim();
				}
			}
			return ret;
		} catch (Exception ex) {
			Logger.getLogger().warn("Couldn't find the newest forge version :(", ex);
		}
		return "";
	}

}
