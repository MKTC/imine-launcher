package nl.makertim.iminelauncher.pack;

public enum PackStatus {

	NEW("not installed"),
	UP_TO_DATE("up-to-date!"),
	OUTDATED("newer version available"),
	OTHER("update?"),
	UNKNOWN("unknown");

	public final String message;

	PackStatus(String message) {
		this.message = message;
	}
}
