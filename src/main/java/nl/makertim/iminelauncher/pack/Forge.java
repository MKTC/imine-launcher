package nl.makertim.iminelauncher.pack;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.OS;
import nl.makertim.iminelauncher.util.FileUtil;
import nl.makertim.iminelauncher.util.ProcessUtil;

public interface Forge {

    String getForgeVersion();

    default String getJarUrl() {
        String version = getForgeVersion();
		if (version.contains("forge")) {
            version = version.split("forge")[1];
		}
		if (version.contains("Forge")) {
			version = version.split("Forge")[0] + version.split("Forge")[1];
        }
        return "http://files.minecraftforge.net/maven/net/minecraftforge/forge/" + version + "/forge-" + version
                + "-installer.jar";
    }

    default void installClient(File installLocation, Consumer<Double> statusUpdate) {
        try {
            File version = new File(FileUtil.getMojangApplicationDataFolder(), "versions/" + getForgeVersion());
            if (!version.exists() || !version.isDirectory()) {
				FileUtils.copyURLToFile(new URL(getJarUrl()), new File(installLocation, "forge.jar"));
                forgeStart(installLocation, "java", "-jar", "forge.jar");
            }
            AbstractPack.copyDefaultConfig(installLocation);
        } catch (IOException ex) {
			Logger.getLogger().error("Something went wrong with installing forge for " + installLocation.getName(), ex);
        }
    }

    default void preInstallServer(File installLocation) {
        File serverFile = new File(installLocation, "serverInstaller.jar");
        try {
            FileUtils.copyURLToFile(new URL(getJarUrl()), serverFile);
        } catch (IOException ex) {
            Logger.getLogger().error("Couldn't installClient server in " + installLocation.getAbsolutePath(), ex);
            return;
        }
        try {
            forgeStart(installLocation, "java", "-jar", "serverInstaller.jar", "--installServer");
        } catch (IOException ex) {
            Logger.getLogger().error("Something went wrong with installing", ex);
        }
    }

    default void postInstallServer(File installLocation) {
        File log = new File(installLocation, "serverInstaller.jar.log");
        File serverFile = new File(installLocation, "serverInstaller.jar");
        if (!log.delete()) {
            log.deleteOnExit();
        }
        if (!serverFile.delete()) {
            serverFile.deleteOnExit();
        }
        OS.getOS().openFolder(installLocation);
    }

    default void forgeStart(File installLocation, String... args) throws IOException {
        BooleanProperty single = new SimpleBooleanProperty(true);
        ProcessUtil.startProcess(installLocation, in -> {
            Logger.getLogger().debug(in);
            if (in.toLowerCase().contains("lzma") && single.get()) {
                single.set(false);
                Logger.getLogger().info("Forge installed");
            }
        }, err -> Logger.getLogger().error(err), args);
        while (single.get()) {
            try {
                Thread.sleep(100L);
            } catch (Exception ex) {
            }
        }
    }
}
