package nl.makertim.iminelauncher.pack;

import java.io.File;
import java.util.Base64;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;

import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.Main;
import nl.makertim.iminelauncher.Version;
import nl.makertim.iminelauncher.util.ImgUtil;

public interface Pack {

	String getId();

	String getImgURL();

	String getName();

	boolean isExternal();

	boolean useHopper();

	File getDirectory();

	Version getLocalVersion();

	Version getNewVersion();

	void backup();

	String getUrl();

	default String getUserUrl() {
		return getUrl();
	}

	PackStatus getStatus();

	String getMinRamSize();

	String getPrefRamSize();

	default String getIcon() {
		try {
			File imgFile = ImgUtil.getCachedImg(getImgURL());
			return "data:image/png;base64," + new String(
					Base64.getEncoder().encode(FileUtils.readFileToByteArray(ImgUtil.getCachedImg(getImgURL()))));
		} catch (Exception ex) {
			ex.printStackTrace();
			Logger.getLogger().warn("Setting icon for vanilla launcher failed.");
			return "Emerald_Block";
		}
	}

	default boolean hasEnoughRam() {
		if (getMinRamSize().endsWith("M")) {
			return Integer.parseInt(getMinRamSize().replace("M", "")) < Main.RAM_MB_SPARE;
		} else if (getMinRamSize().endsWith("G")) {
			return Integer.parseInt(getMinRamSize().replace("G", "")) < Main.RAM_GB_SPARE;
		}
		return false;
	}

	default boolean hasPrefRam() {
		if (getPrefRamSize().endsWith("M")) {
			return Integer.parseInt(getPrefRamSize().replace("M", "")) < Main.RAM_MB_SPARE;
		} else if (getPrefRamSize().endsWith("G")) {
			return Integer.parseInt(getPrefRamSize().replace("G", "")) < Main.RAM_GB_SPARE;
		}
		return false;
	}

	void installClient(File installLocation, Consumer<Double> statusUpdate);

	void removeClient(File from);

	void installServer(File installLocation, Consumer<Double> statusUpdate);

}
