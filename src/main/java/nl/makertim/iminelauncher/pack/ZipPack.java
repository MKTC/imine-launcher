package nl.makertim.iminelauncher.pack;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.function.Consumer;

import nl.makertim.iminelauncher.util.FileUtil;
import org.apache.commons.io.FileUtils;

import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.Version;
import nl.makertim.iminelauncher.mojang.LauncherProfileInjector;
import nl.makertim.iminelauncher.util.WebUtil;

public class ZipPack extends AbstractPack implements Forge {

	private final String zipUrl;
	private final String forgeVersion;
	private final String version;

	public ZipPack(String id, String name, String zipUrl, String version, String imgURL, String forgeVersion, String minRamSize, String prefRamSize) {
		super(id, name, imgURL, minRamSize, prefRamSize, true, false);
		this.zipUrl = zipUrl;
		this.forgeVersion = forgeVersion;
		this.version = version;
	}

	@Override
	public String getForgeVersion() {
		return forgeVersion;
	}

	@Override
	public Version getLocalVersion() {
		try {
			return new Version(
					FileUtils.readFileToString(new File(getDirectory(), "version.txt"), Charset.defaultCharset()));
		} catch (IOException ex) {
			Logger.getLogger().warn("Couldn't get version of " + getName(), ex);
			return new Version("0");
		}
	}

	@Override
	public Version getNewVersion() {
		try {
			return new Version(version);
		} catch (IllegalArgumentException ex) {
			return new Version("0");
		}
	}

	@Override
	public String getUrl() {
		return zipUrl;
	}

	@Override
	public String getUserUrl() {
		return getUrl().replace(".git", "");
	}

	@Override
	public PackStatus getStatus() {
		File packFolder = getDirectory();
		if (packFolder.exists()) {
			Version current = getLocalVersion();
			int version = current.compareTo(getNewVersion());
			if (version < 0) {
				return PackStatus.OUTDATED;
			} else if (version == 0) {
				return PackStatus.UP_TO_DATE;
			}
		} else {
			return PackStatus.NEW;
		}
		return PackStatus.UNKNOWN;
	}

	@Override
	public void installClient(File installLocation, Consumer<Double> statusUpdate) {
		statusUpdate.accept(0.2);
		Forge.super.installClient(installLocation, statusUpdate);
		statusUpdate.accept(0.4);
		downloadPack(installLocation, statusUpdate);
	}

	@Override
	public void removeClient(File file) {
		try {
			FileUtils.deleteDirectory(file);
			LauncherProfileInjector lpi = new LauncherProfileInjector(this);
			lpi.removePack();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void installServer(File installLocation, Consumer<Double> statusUpdate) {
		preInstallServer(installLocation);
		downloadPack(installLocation, statusUpdate);
		postInstallServer(installLocation);
	}

	private void downloadPack(File installLocation, Consumer<Double> statusUpdate) {
		new Thread(() -> {
			for (double d = 0.4; d < 0.8; d += 0.003) {
				try {
					Thread.sleep(100);
				} catch (Exception e) {
				}
				statusUpdate.accept(d);
			}
		}).start();
		try {
			File zipFile = new File(installLocation, "pack.zip");
			FileUtils.copyURLToFile(new URL(zipUrl), zipFile);
			FileUtil.unzip(zipFile);
			if (!zipFile.delete()) {
				zipFile.deleteOnExit();
			}
		} catch (IOException ex) {
			Logger.getLogger().error("Something went wrong with downloading the mods", ex);
		}
	}
}
