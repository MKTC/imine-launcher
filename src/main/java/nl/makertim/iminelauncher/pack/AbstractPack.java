package nl.makertim.iminelauncher.pack;

import com.google.gson.GsonBuilder;
import nl.makertim.iminelauncher.mojang.LauncherProfileInjector;
import nl.makertim.iminelauncher.util.FileUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public abstract class AbstractPack implements Pack {

	private final String id;
	private final String name;
	private final String imgURL;
	private final String minRamSize;
	private final String prefRamSize;
	private final boolean isExternal;
	private final boolean useHopper;

	public AbstractPack(String id, String name, String imgURL, String minRamSize, String prefRamSize, boolean isExternal, boolean useHopper) {
		this.id = id;
		this.name = name;
		this.imgURL = imgURL;
		this.minRamSize = minRamSize;
		this.prefRamSize = prefRamSize;
		this.isExternal = isExternal;
		this.useHopper = useHopper;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getImgURL() {
		return imgURL;
	}

	@Override
	public String getMinRamSize() {
		return minRamSize;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getPrefRamSize() {
		return prefRamSize;
	}

	@Override
	public boolean isExternal() {
		return isExternal;
	}

	@Override
	public boolean useHopper() {
		return false;
	}

	@Override
	public void backup() {
		if (getDirectory().exists()) {
			File saveDir = new File(getDirectory(), "saves");
			if (saveDir.exists()) {
				File backupDir = new File(getDirectory(), "_saves_" + getLocalVersion());
				if (backupDir.exists()) {
					try {
						FileUtils.cleanDirectory(backupDir);
					} catch (IOException ez) {
						ez.printStackTrace();
					}
				}
				try {
					FileUtils.copyDirectory(saveDir, backupDir);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	@Override
	public File getDirectory() {
		if (isExternal()) {
			return new File(FileUtil.getApplicationDataFolder(), getId());
		} else {
			return FileUtil.getMojangApplicationDataFolder();
		}
	}

	@Override
	public String toString() {
		return new GsonBuilder().create().toJson(this);
	}

	public static void copyDefaultConfig(File installLocation) throws IOException {
		File[] originalFiles = new File[]{new File(FileUtil.getMojangApplicationDataFolder(), "options.txt"),
				new File(FileUtil.getMojangApplicationDataFolder(), "servers.dat")};
		for (File originalFile : originalFiles) {
			File newFile = new File(installLocation, originalFile.getName());
			if (newFile.exists()) {
				continue;
			}
			FileUtils.copyFile(originalFile, newFile);
		}
	}
}
