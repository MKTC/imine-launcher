package nl.makertim.iminelauncher.pack;

import nl.makertim.iminelauncher.Logger;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.merge.MergeStrategy;
import org.eclipse.jgit.transport.URIish;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

class Git {
	private static final double STEPS = 1D / 8D;

	static void downloadPack(GitPack pack, File output, Consumer<Double> statusUpdate) throws IOException {
		String gitUrl = pack.getUrl();
		try {
			double step = 1;
			Logger.getLogger().info("Downloading pack");
			Logger.getLogger().debug("Setting up git");
			statusUpdate.accept(STEPS * step++);
			org.eclipse.jgit.api.Git git;
			if (!new File(output, ".git").exists()) {
				git = org.eclipse.jgit.api.Git.init().setDirectory(output).call();
			} else {
				git = org.eclipse.jgit.api.Git.open(output);
			}
			statusUpdate.accept(STEPS * step++);
			URIish uri = new URIish(gitUrl);
			Logger.getLogger().debug("\t- Remote");
			if (git.getRepository().getRemoteNames().contains("origin")) {
				RemoteSetUrlCommand remoteSet = git.remoteSetUrl();
				remoteSet.setName("origin");
				remoteSet.setUri(uri);
				remoteSet.call();
				Logger.getLogger().debug("\t  secure link");
			} else {
				RemoteAddCommand remoteAdd = git.remoteAdd();
				remoteAdd.setName("origin");
				remoteAdd.setUri(uri);
				remoteAdd.call();
				Logger.getLogger().debug("\t  setup link");
			}
			statusUpdate.accept(STEPS * step++);
			Logger.getLogger().debug("\t- hacky set default remote");
			// git branch --set-upstream-to=origin/master
			StoredConfig config = git.getRepository().getConfig();
			config.setString("branch", "master", "remote", "origin");
			config.setString("branch", "master", "merge", "refs/heads/master");
			// git config core.autocrlf true
			config.setString("core", null, "autocrlf", "true");
			config.save();
			statusUpdate.accept(STEPS * step++);
			if (!git.status().call().isClean()) {
				Logger.getLogger().debug("\t- Reset --hard");
				ResetCommand reset = git.reset();
				reset.setMode(ResetCommand.ResetType.HARD);
				reset.call();
			}
			double steps = step++;
			new Thread(() -> {
				for (double d = STEPS * steps; d < STEPS * (steps + 1); d += 0.003) {
					try {
						Thread.sleep(100);
					} catch (Exception e) {
					}
					statusUpdate.accept(d);
				}
			}).start();
			Logger.getLogger().info("Downloading...");
			Logger.getLogger().debug("\t- Pull");
			PullCommand pull = git.pull();
			pull.setRemote("origin");
			pull.setRemoteBranchName("master");
			pull.setStrategy(MergeStrategy.THEIRS);
			statusUpdate.accept(STEPS * step++);
			PullResult pullResult = pull.call();
			statusUpdate.accept(STEPS * step++);
			if (!pullResult.isSuccessful()) {
				Logger.getLogger().warn("Something went wrong downloading git!");
			}
		} catch (CheckoutConflictException ex) {
			for (String conflicting : ex.getConflictingPaths()) {
				File conflictingFile = new File(output, conflicting);
				if (!conflictingFile.delete()) {
					Logger.getLogger().error("You need to delete: " + conflictingFile.getAbsolutePath());
					throw new IOException(ex);
				}
				Logger.getLogger().info("Removed '" + conflicting + "' because it has been changed by me.");
			}
			downloadPack(pack, output, statusUpdate);
		} catch (Exception ex) {
			Logger.getLogger().error("failed to download pack");
			throw new IOException(ex);
		}
	}
}
