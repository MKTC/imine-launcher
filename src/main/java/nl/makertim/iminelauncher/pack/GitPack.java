package nl.makertim.iminelauncher.pack;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.function.Consumer;

import org.apache.commons.io.FileUtils;

import nl.makertim.iminelauncher.Logger;
import nl.makertim.iminelauncher.Version;
import nl.makertim.iminelauncher.mojang.LauncherProfileInjector;
import nl.makertim.iminelauncher.util.WebUtil;

public class GitPack extends AbstractPack implements Forge {

	private final String gitUrl;
	private final String forgeVersion;
	private final String versionUrl;

	public GitPack(String id, String name, String gitUrl, String versionUrl, String imgURL, String forgeVersion, String minRamSize, String prefRamSize) {
		super(id, name, imgURL, minRamSize, prefRamSize, true, false);
		this.gitUrl = gitUrl;
		this.forgeVersion = forgeVersion;
		this.versionUrl = versionUrl;
	}

	private String getVersionURL() {
		return versionUrl;
	}

	@Override
	public String getForgeVersion() {
		return forgeVersion;
	}

	@Override
	public Version getLocalVersion() {
		try {
			File version = new File(getDirectory(), "version.txt");
			if (version.exists()) {
				return new Version(
						FileUtils.readFileToString(new File(getDirectory(), "version.txt"), Charset.defaultCharset()));
			} else {
				throw new IOException("version.txt not found");
			}
		} catch (IOException ex) {
			Logger.getLogger().warn("Couldn't get version of " + getName(), ex);
			return new Version("0");
		}
	}

	@Override
	public Version getNewVersion() {
		try {
			return new Version(WebUtil.getResponse(getVersionURL()));
		} catch (IllegalArgumentException ex) {
			return new Version("0");
		}
	}

	@Override
	public String getUrl() {
		return gitUrl;
	}

	@Override
	public String getUserUrl() {
		return getUrl().replace(".git", "");
	}

	@Override
	public PackStatus getStatus() {
		try {
			File packFolder = getDirectory();
			if (!packFolder.exists()) {
				return PackStatus.NEW;
			}
			if (!new File(packFolder, ".git").exists()) {
				return PackStatus.UNKNOWN;
			}
			Version current = getLocalVersion();
			int version = current.compareTo(getNewVersion());
			if (version < 0) {
				return PackStatus.OUTDATED;
			} else if (version == 0) {
				return PackStatus.UP_TO_DATE;
			}
		} catch (Exception ex) {
			Logger.getLogger().error("Status of pack " + getName() + " unknown", ex);
			return PackStatus.UNKNOWN;
		}
		return PackStatus.UNKNOWN;
	}

	@Override
	public void installClient(File installLocation, Consumer<Double> statusUpdate) {
		Forge.super.installClient(installLocation, statusUpdate);
		try {
			Git.downloadPack(this, installLocation, statusUpdate);
		} catch (IOException ex) {
			Logger.getLogger().error("Error while downloading git", ex);
		}
	}

	@Override
	public void removeClient(File file) {
		try {
			File backupDir = new File(file.getParentFile(), "backup_" + getId());
			if (backupDir.exists()) {
				FileUtils.deleteDirectory(backupDir);
			}
			FileUtils.moveDirectory(file, backupDir);
			FileUtils.deleteDirectory(file);
			LauncherProfileInjector lpi = new LauncherProfileInjector(this);
			lpi.removePack();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void installServer(File installLocation, Consumer<Double> statusUpdate) {
		preInstallServer(installLocation);
		try {
			Git.downloadPack(this, installLocation, statusUpdate);
		} catch (IOException ex) {
			Logger.getLogger().error("Something went wrong with downloading the mods", ex);
		}
		postInstallServer(installLocation);
	}
}
