# Version 0.3.3

New launcher = new stuff  
Al though it will break at many places, i try to have keep support for the old launcher  
But, now first; new support for selected user, you can switch accounts again  
and made a dirty fix for selected pack, since mojang removed this feature.  

ADDED:  
- Launcher icons should now be from the pack, if that fails it will be a emerald block or leaves in the new launcher

FIXED:  
- Selecting users was broken  
- User images seem to be broken sometimes  
- Selected pack should be a bit better; it now keeps track of the last pack selected by me & old launcher

# Version 0.3.2

Some of us got the new launcher of minecraft, i got it now  
so, now we have icons in that launcher, i use that  

ADDED:  
- An icon in the vanilla launcher, for now all they are emerald blocks    

# Version 0.3.1

Because some users are working on Apple / Linux, I made a fix for them  

FIXED:  
- Alerts / Dialogs no longer crash you, they just warn you in console.    

# Version 0.3.0

Rerouted some web stuff  
this was needed because the host websrver has updated there security system and this program was not allowed to do webrequest anymore      

but now that is fixed ^.^  

ADDED:  
- cache pack images  
- hiding of mails of users, because it was requested  

FIXED:  
- Selected packs are now more noticeable  

# Version 0.2.7

ADDED:  
- Zip packs  
- Tetrachate (And support for al modpacks that use a ZIP structure)  
- Animated the progressbar while downloading    

FIXED:  
- Calling every update a force update  
- Installing Vanilla & Clean Forge server  
- Selected pack not always in scroll  
- Spacing that progressbar would disappear  

# Version 0.2.6

ADDED:  
- Backup saves on update  
- Server installer  
- Faster scroll for packs  

FIXED:  
- Pack not selecting propperly after closing minecraft launcher

# Version 0.2.5

ADDED:  
- Vanilla as Pack  
- Forge as Pack  
- Keep pack that was last selected as selected  
- Info of a pack (*Simply right click it)  
- Added a changelog of a pack visible  
- Option menu (for changing install location)  
- Add other packs by url  
- Uninstall a pack  
- Application icons on all screens  
- A scrollbar to packs  
- Select users from launcher

FIXED:  
- Launcher not working as great on clean install  
- Fixed bug on fast computers that forge was not installed but pack was launching

# Version 0.2.4

FIXED:  
- Hotfix for packs not selecting vanilla launcher  
- Download new launcher button only showing after resize of console

# Version 0.2.3

ADDED:  
- Console download button

FIXED:  
- Crash on no internet, now shows empty launcher...

# Version 0.2.2

ADDED:  
- A console, so people can click it away

# Version 0.2.1

FIXED:  
- Minecraft launcher not selecting new forge version  
- Packs not updating their forge versions

# Version 0.2.0

CHANGED:  
- Changed the way how packs are downloaded (packs are responsible about their configs)

# Version 0.1.4
  
ADDED:  
- new installations will copy options/servers  
- change installation directory (ui not done yet)  
  
FIX:  
- Java args resetting after update  
  

# Version 0.1.3  
  
FIX:  
- Packs not starting because of ram  
- Some of the wired web issues  
  

# Version 0.1.2  
  
FIX:  
- Resizing of launcher leaves white spacings  
  

# Version 0.1.1  
  
FIX:  
- Site now loading  
- Update Minecraft Launcher and keep it up to date. (Support for windows-only launchers)  


# Version 0.1.0

Launcher his first public release!  
This launcher is made in 3 days, so bugs and other non working features expected  

Bugs are always welcome at Twitter @[MakerTim](https://twitter.com/MakerTim)  
or on our bug track page [bugs.imine.nl](http://bugs.imine.nl/my_view_page.php).


# Good news!

Your launcher is working!  
If you can read this text means that the launcher is working as intended  

Click on a modpack on the right side and follow the instructions on the bottom  
Enyoy!
